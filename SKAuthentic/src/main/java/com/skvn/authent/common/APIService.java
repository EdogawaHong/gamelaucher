package com.skvn.authent.common;

import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.ChangePassData;
import com.skvn.authent.model.req.ForgotPassword;
import com.skvn.authent.model.req.Login;
import com.skvn.authent.model.req.LoginGuest;
import com.skvn.authent.model.req.Logout;
import com.skvn.authent.model.req.RegisterData;
import com.skvn.authent.model.req.ReqUserInfo;
import com.skvn.authent.model.req.ResetPassword;
import com.skvn.authent.model.req.SessionChecking;
import com.skvn.authent.model.req.SetPassData;
import com.skvn.authent.model.req.VerifyOtp;
import com.skvn.authent.model.req.iap.CreateTransaction;
import com.skvn.authent.model.res.LoginData;
import com.skvn.authent.model.res.ResReqForgot;
import com.skvn.authent.model.res.SessionCheckingResult;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.model.res.iap.Products;
import com.skvn.authent.model.stackover.SOAnswersResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @GET("/answers?order=desc&sort=activity&site=stackoverflow")
    Call<List<SOAnswersResponse>> getAnswers();

    @POST("user/changePass")
    Call<BaseResponse<LoginData>> changePass(@Body() ChangePassData register, @Header("Authorization") String authHeader);

    @POST("user/register")
    Call<BaseResponse<LoginData>> register(@Body() RegisterData register);

    @POST("user/login/skw")
    Call<BaseResponse<LoginData>> loginSKW(@Body() Login login);

    @GET("user/login/google")
    Call<BaseResponse<LoginData>> loginGoogle(@Query("code") String code,
                                              @Query("gameId") String gameId,
                                              @Query("appKey") String appKey,
                                              @Query("deviceInfo") String deviceInfo);

    @GET("user/login/facebook")
    Call<BaseResponse<LoginData>> loginFacebook(@Query("code") String code,
                                              @Query("gameId") String gameId,
                                              @Query("appKey") String appKey,
                                              @Query("deviceInfo") String deviceInfo);

    @POST("user/login/guest")
    Call<BaseResponse<LoginData>> loginGuest(@Body() LoginGuest guestInfo);

    @POST("user/verifySession")
    Call<BaseResponse<SessionCheckingResult>> checkSession(@Header("Authorization") String authHeader, @Query("versionCode") String versionCode,@Query("appKey") String appKey);

    @POST("user/logout")
    Call<BaseResponse<SessionCheckingResult>> logout(@Body() Logout logout, @Header("Authorization") String authHeader);

    @GET("user/info")
    Call<BaseResponse<UserInfo>> getUserInfo(@Header("Authorization") String authHeader);

    @POST("user/info/update")
    Call<BaseResponse<String>> updateInfo(@Header("Authorization") String authHeader, @Body ReqUserInfo req);

    @POST("user/addPassword")
    Call<BaseResponse<String>> addPassword(@Header("Authorization") String authHeader, @Body SetPassData setPassData);

    @GET("user/email/add/sendOtp")
    Call<BaseResponse<String>> sendOtpEmail(@Header("Authorization") String authHeader, @Query("email") String email);

    @POST("user/email/change/sendOldOtp")
    Call<BaseResponse<String>> sendOtpOldEmailChange(@Header("Authorization") String authHeader, @Query("email") String email, @Body VerifyOtp verifyOtp);

    @POST("user/email/change/sendNewOtp")
    Call<BaseResponse<String>> sendOtpNewEmailChange(@Header("Authorization") String authHeader, @Query("email") String email, @Body VerifyOtp verifyOtp);

    @POST("user/email/verifyOtp")
    Call<BaseResponse<String>> verifyOtp(@Header("Authorization") String authHeader, @Body VerifyOtp verifyOtp);

    @POST("user/verifyOtpPassword")
    Call<BaseResponse<String>> verifyOtpPass(@Header("Authorization") String authHeader, @Body VerifyOtp verifyOtp);

    @POST("user/email/resendOtp")
    Call<BaseResponse<String>> resendOtp(@Header("Authorization") String authHeader, @Body VerifyOtp verifyOtp);

    @POST("user/resendOtpForgetPassword")
    Call<BaseResponse<String>> resendOtpForgotPass(@Header("Authorization") String authHeader, @Body VerifyOtp verifyOtp);

    @POST("user/forgetPassword")
    Call<BaseResponse<ResReqForgot>> forgotPass(@Body ForgotPassword forgot);

    @POST("user/resetPassword")
    Call<BaseResponse<String>> resetPassword(@Header("Authorization") String authHeader, @Body ResetPassword forgot);

    @POST("transaction/create")
    Call<BaseResponse<String>> createdTransaction(@Header("Authorization") String authHeader, @Body CreateTransaction transaction);

    @GET("transaction/updatePaymentStatus")
    Call<BaseResponse<String>> updatedTransaction(@Header("Authorization") String authHeader,
                                                  @Query("purchaseType") String purchaseType,
                                                  @Query("packageName") String packageName,
                                                  @Query("productId") String productId,
                                                  @Query("purchaseToken") String purchaseToken,
                                                  @Query("password") String password,
                                                  @Query("receptData") String receptData,
                                                  @Query("errorCode") String code,
                                                  @Query("transId") String transId,
                                                  @Query("message") String message
    );

    @GET("transaction/listProduct")
    Call<BaseResponse<List<Products>>> listproduct(@Query("packageName") String packageName);
}