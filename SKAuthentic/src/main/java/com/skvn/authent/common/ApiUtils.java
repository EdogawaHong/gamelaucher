package com.skvn.authent.common;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.skvn.authent.model.base.Error;

public class ApiUtils {
 
    public static final String BASE_VER_CODE_ANDROID = "1";
    public static final String BASE_URL = "https://api.launcher.skw.vn/";

    public static APIService getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }

    public static boolean notError(Error error, Context context) {
        if (error.getCode() == 0)
            return true;
        else if (error.getCode() == 3) {
            Utils.showToast(context, error.getMessage());
            SharedUtils.getInstance(context).clearData();
            context.sendBroadcast(new Intent(AppConstance.LOGOUT));
            return false;
        } else if (error.getCode() == 333){
            showDialog(context);
            return false;
        }
        else{
//            Utils.showToast(context, error.getMessage());
            return false;
        }
    }

    public static String getHeaderAuthorization(Context context){
        String token = SharedUtils.getInstance(context).getStringValue(AppConstance.TOKEN);
        return "Bearer " + token;
    }

    static void showDialog(Context context){
        new AlertDialog.Builder(context)
                .setTitle("Thông báo")
                .setMessage("Đã có phiên bản mới của ứng dụng.\nVui lòng cập nhật ngay!")
                .setCancelable(false)
                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Cập nhật", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delegate operation
                        moveToStore(context);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    static void moveToStore(Context context){
        final String appPackageName = context.getPackageName();

        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}