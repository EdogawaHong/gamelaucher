package com.skvn.authent.common;

public class AppConstance {
    public static final String CGUUID = "cguuid";
    public static final String SESSION = "session";
    public static final String LOGOUT = "logout";
    public static final String TOKEN = "token";
    public static final String USER_DATA = "user_data";
    public static final String USER_INFO = "user_info";
    public static final String UPDATE_USER_INFO = "updateUserInfo";
    public static final String LOAD_USER_INFO = "load_user_info";
    public static final String LOGIN_GUEST = "login_guest";
    public static final String LOGIN_GOOGLE = "login_google";
    public static final String LOGIN_FACEBOOK = "login_facebook";
    public static final String CALLBACK_RESPONSE = "CALLBACK_RESPONSE";
    public static final String REGISTER_RESPONSE = "REGISTER_RESPONSE";
    public static final String USER_NAME = "USER_NAME";
//    public static final String MAIN_ACTIVITY_LAUNCHER = "MAIN_ACTIVITY_LAUNCHER";
    public static final String SKU_RES = "SKU_RES";

    public static final String GAME_ID = "2";

    public static String formatDate = "dd/MM/yyyy";
    public static String formatDateTime = "dd/MM/yyyy hh:mm:ss";


    public static final String ACTION_LOGIN_SUCCESS = "LOGIN_SUCCESS";
    public static final String ACTION_BACK = "ACTION_BACK";
    public static final String ACTION_LOGOUT = "ACTION_LOGOUT";


    public static final String TOKEN_INVALID = "TOKEN_INVALID";


}
