package com.skvn.authent.common;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.skvn.authent.view.headService.LifeCycleReceiver;

public class AppLifecycleHandler implements Application.ActivityLifecycleCallbacks {
    // I use four separate variables here. You can, of course, just use two and
    // increment/decrement them instead of using four and incrementing them all.
    private int resumed;
    private int paused;
    private int started;
    private int stopped;
    private static final String TAG = "AppLifecycleHandler";

    private String tmpMainActivity = "";

//    String getMainActivityLauncher(Activity activity) {
//        return SharedUtils.getInstance(activity).getStringValue(AppConstance.MAIN_ACTIVITY_LAUNCHER);
//    }

    static void callLauncher(Activity app, boolean show) {
        if (show) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(app)) {
                    app.sendBroadcast(new Intent(app, LifeCycleReceiver.class).putExtra("running", show));
                }
            } else {
                app.sendBroadcast(new Intent(app, LifeCycleReceiver.class).putExtra("running", show));
            }
        } else {
            app.sendBroadcast(new Intent(app, LifeCycleReceiver.class).putExtra("running", show));
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
//        tmpMainActivity = getMainActivityLauncher(activity);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        String current_activity = activity.getLocalClassName();
        Log.e(TAG, "onActivityResumed: " + current_activity);
//        String main_activity = getMainActivityLauncher(activity);
//        if (tmpMainActivity != null && current_activity != null) {
//
//        }
        try {
            if (tmpMainActivity.equals(current_activity))
                callLauncher(activity, true);
        } catch (Exception e){

        }
        ++resumed;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.e(TAG, "onActivityPaused: " + activity.getLocalClassName());
        callLauncher(activity, false);
        ++paused;
        android.util.Log.w("test", "application is in foreground: " + (resumed > paused));
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        ++started;
    }

    @Override
    public void onActivityStopped(Activity activity) {
        ++stopped;
        android.util.Log.w("test", "application is visible: " + (started > stopped));
    }

    // If you want a static function you can use to check if your application is
    // foreground/background, you can use the following:
    /*
    // Replace the four variables above with these four
    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;

    // And these two public static functions
    public static boolean isApplicationVisible() {
        return started > stopped;
    }

    public static boolean isApplicationInForeground() {
        return resumed > paused;
    }
    */
}