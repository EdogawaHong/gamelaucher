package com.skvn.authent.common;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedUtils {
    private static SharedUtils mInstance = null;

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private static final String SHARED_NAME = "sync_name";

    public static synchronized SharedUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedUtils();
            sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }

        return mInstance;
    }

    public void putStringValue(String name, String value) {
        editor.putString(name, value);
        editor.apply();
    }

    public String getStringValue(String name) {
        return sharedPreferences.getString(name, null);
    }

    public String getStringValue(String name, String value) {
        return sharedPreferences.getString(name, value);
    }

    public void putLongValue(String name, Long value) {
        editor.putLong(name, value);
        editor.apply();
    }

    public Long getLongValue(String name) {
        return sharedPreferences.getLong(name, -1);
    }

    public Long getLongValue(String name, Long value) {
        return sharedPreferences.getLong(name, value);
    }

    public void putIntValue(String name, int value) {
        editor.putInt(name, value);
        editor.apply();
    }

    public int getIntValue(String name) {
        return sharedPreferences.getInt(name, -1);
    }

    public int getIntValue(String name, int value) {
        return sharedPreferences.getInt(name, value);
    }

    public void putBooleanValue(String name, boolean value) {
        editor.putBoolean(name, value);
        editor.apply();
    }

    public boolean getBooleanValue(String name) {
        return sharedPreferences.getBoolean(name, false);
    }

    public void clearData() {
        editor.clear();
        editor.apply();
    }
}