package com.skvn.authent.common;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.skvn.authent.R;
import com.skvn.authent.model.data.DeviceInfo;
import com.skvn.authent.view.dialog.DialogLoading;

import java.util.List;
import java.util.UUID;

import static android.content.Context.TELEPHONY_SERVICE;

public class Utils {

    public static void hideSystemBar(Activity activity) {
        View decorView = activity.getWindow().getDecorView();
// Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = activity.getActionBar();
        if (actionBar != null)
            actionBar.hide();
    }

    public static boolean appInForeground(@NonNull Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }

        for (ActivityManager.RunningAppProcessInfo runningAppProcess : runningAppProcesses) {
            if (runningAppProcess.processName.equals(context.getPackageName()) &&
                    runningAppProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String hideEmail(String email) {
        return email.replaceAll("(?<=.{4}).(?=.*@)", "*");
    }

    public static String hidePhone(String phone) {
        return phone.replaceAll("(?<=.{5}).(?=.*)", "*");
    }

    public static boolean checkPassWord(Context context, String pass, String confirm) {
        if (pass.length() >= 6 && confirm.length() >= 6) {
            if (pass.trim().equals(confirm.trim())) {
                return true;
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.not_match_password), Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            Toast.makeText(context, context.getResources().getString(R.string.not_lenght_password), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static DeviceInfo getDeviceInfo() {
        String CODENAME = Build.VERSION.CODENAME;
        String RELEASE = Build.VERSION.RELEASE;
        String INCREMENTAL = Build.VERSION.INCREMENTAL;
//        int PREVIEW_SDK_INT = Build.VERSION.PREVIEW_SDK_INT;
        int SDK_INT = Build.VERSION.SDK_INT;
        String DEVICE = Build.DEVICE;

        Log.e("Utils", "DeviceInfo: " + CODENAME + " " + RELEASE + " " + INCREMENTAL + " " + SDK_INT + " " + DEVICE);

        return new DeviceInfo(CODENAME, RELEASE, INCREMENTAL, SDK_INT, DEVICE);
    }

    public static String getUUID(Context context) {
        String uuid = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("Utils", "UUID: " + uuid);
        return uuid;
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, CustomToast.LENGTH_SHORT).show();
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, CustomToast.LENGTH_LONG).show();
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

}


