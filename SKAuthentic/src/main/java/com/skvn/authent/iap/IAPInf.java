package com.skvn.authent.iap;

import com.skvn.authent.model.res.iap.Products;

import java.util.List;

public interface IAPInf {
    void showLoading(boolean show);
    <T> void onCreatedTransaction(boolean success, String message, T data);
    <T> void onupdatedTransaction(boolean success, String message, T data, boolean paymentSuccess);

    void onGetSuccess(boolean success, String message, List<Products> data);
}
