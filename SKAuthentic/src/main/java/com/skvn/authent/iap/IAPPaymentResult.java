package com.skvn.authent.iap;

public interface IAPPaymentResult {
    void notFoundSku();
    void onCancel();
    void onSuccess();
    void onFailed(String message);
}
