package com.skvn.authent.iap;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.ChangePasswordInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.ForgotPassword;
import com.skvn.authent.model.req.iap.CreateTransaction;
import com.skvn.authent.model.res.ResReqForgot;
import com.skvn.authent.model.res.iap.Products;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IAPPresenter {
    Context context;
    IAPInf inf;

    public IAPPresenter(Context context, IAPInf inf) {
        this.context = context;
        this.inf = inf;
    }

//    public void loadListProduct() {
//        inf.showLoading(true);
////        String authorization = ApiUtils.getHeaderAuthorization(context);
//        ApiUtils.getAPIService().listproduct(context.getPackageName()).enqueue(new Callback<BaseResponse<List<Products>>>() {
//            @Override
//            public void onResponse(Call<BaseResponse<List<Products>>> call, Response<BaseResponse<List<Products>>> response) {
//                inf.showLoading(false);
//                if (response.isSuccessful()) {
//                    if (ApiUtils.notError(response.body().getError(), context)) {
//                        inf.onGetSuccess(true, response.body().getError().getMessage(), response.body().getData());
//                    } else {
//                        inf.onGetSuccess(false, response.body().getError().getMessage(), null);
//                    }
//                } else {
//                    String message = "";
//                    // error case
//                    switch (response.code()) {
//                        case 404:
//                            message = "Not found";
//
//                            break;
//                        case 500:
//                            message = "Server broken";
//                            break;
//                        default:
//                            message = "Unknown error";
//                            break;
//                    }
//                    inf.onGetSuccess(false, "Không thể kết nối đến máy chủ!", null);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<BaseResponse<List<Products>>> call, Throwable t) {
//                inf.showLoading(false);
//                inf.onGetSuccess(false, "Không thể kết nối đến máy chủ!", null);
//            }
//        });
//    }

    public void createdTransaction(String product_id, String user_id, String gameClientTransId, String serverId) {
        inf.showLoading(true);
        CreateTransaction transaction = new CreateTransaction();
        transaction.setPaymentChannel("GOOGLE_PLAY");
        transaction.setDeviceInfo(Utils.getUUID(context));
        transaction.setProductId(product_id);
        transaction.setServerId(serverId);
        transaction.setUserId(user_id);
        transaction.setGameClientTransId(gameClientTransId);
        String authorization = ApiUtils.getHeaderAuthorization(context);
        ApiUtils.getAPIService().createdTransaction(authorization, transaction).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                inf.showLoading(false);
                if (response.isSuccessful()) {
                    if (ApiUtils.notError(response.body().getError(), context)) {
                        inf.onCreatedTransaction(true, response.body().getError().getMessage(), response.body().getData());
                    } else {
                        inf.onCreatedTransaction(false, response.body().getError().getMessage(), null);
                    }
                } else {
                    String message = "";
                    // error case
                    switch (response.code()) {
                        case 404:
                            message = "Not found";

                            break;
                        case 500:
                            message = "Server broken";
                            break;
                        default:
                            message = "Unknown error";
                            break;
                    }
                    inf.onCreatedTransaction(false, "Không thể kết nối đến máy chủ!", null);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                inf.showLoading(false);
                inf.onCreatedTransaction(false, t.getMessage(), null);
            }
        });
    }

    public void updatedTransaction(String product_id, String purchaseToken, boolean paymentSuccess, String code, String message, String tmpTransId) {
        inf.showLoading(true);
        String authorization = ApiUtils.getHeaderAuthorization(context);
        try{
            ApiUtils.getAPIService().updatedTransaction(
                    authorization,
                    "GOOGLE_PLAY",
                    context.getPackageName(),
                    product_id,
                    purchaseToken,
                    null,
                    null,
                    code, tmpTransId, message).enqueue(new Callback<BaseResponse<String>>() {
                @Override
                public void onResponse(@NonNull Call<BaseResponse<String>> call, @NonNull Response<BaseResponse<String>> response) {
                    inf.showLoading(false);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (ApiUtils.notError(response.body().getError(), context)) {
                                inf.onupdatedTransaction(true, response.body().getError().getMessage(), response.body().getData(), paymentSuccess);
                            } else {
                                inf.onupdatedTransaction(false, response.body().getError().getMessage(), null, paymentSuccess);
                            }
                        } else {
                            inf.onupdatedTransaction(false, "Cant parse response", null, paymentSuccess);
                        }
                    } else {
                        String message = "";
                        // error case
                        switch (response.code()) {
                            case 404:
                                message = "Not found";
                                break;
                            case 500:
                                message = "Server broken";
                                break;
                            default:
                                message = "Unknown error";
                                break;
                        }
                        inf.onupdatedTransaction(false, "Không thể kết nối đến máy chủ!", null, paymentSuccess);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BaseResponse<String>> call, @NonNull Throwable t) {
                    inf.onupdatedTransaction(false, t.getMessage(), null, paymentSuccess);
                    inf.showLoading(false);
                }
            });
        }catch (Exception e){
            Log.e(TAG, "updatedTransaction: " + e.getLocalizedMessage());
        }
    }

    private static final String TAG = "IAPPresenter";
}
