package com.skvn.authent.iap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.skvn.authent.common.JsonHelper;
import com.skvn.authent.common.Utils;
import com.skvn.authent.model.res.iap.Products;
import com.skvn.authent.view.dialog.DialogLoading;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("StaticFieldLeak")
public class IAPUtils {
    private Activity context;
    private BillingClient billingClient;
    private static final String TAG = "IAPUtils";
    static List<SkuDetails> skuDetails = new ArrayList<>();
    static List<String> skuList = new ArrayList<>();

    static IAPUtils instance;
    private IAPPaymentResult listenerSDk;
    private IAPPresenter presenter;
    private DialogLoading loading;

    private String tmpProductCode;
    private String tmpTransaction;
    private String userId;

    private IAPUtils() {
    }

    boolean isLoadedListProduct = false;

    public static IAPUtils getInstance() {
        if (IAPUtils.instance == null) {
            instance = new IAPUtils();
        }
        return instance;
    }

    public void initializingIAP(Activity context, List<String> skuList) {
        this.context = context;
        loading = new DialogLoading(context);
        this.skuList = skuList;
        presenter = new IAPPresenter(context, new IAPInf() {
            @Override
            public void showLoading(boolean show) {
                if (show) {
                    loading.show();
                } else {
                    loading.dismiss();
                }
            }

            @Override
            public <T> void onCreatedTransaction(boolean success, String message, T data) {
                if (success) {
                    if (data != null && data instanceof String) {
                        tmpTransaction = (String) data;
                    }
                    Utils.showToast(context, message);
                    SkuDetails skuDetail = null;
                    for (SkuDetails detail : skuDetails) {
                        if (detail.getSku().equals(tmpProductCode)) {
                            skuDetail = detail;
                            break;
                        }
                    }
                    if (skuDetail == null) {
                        listenerSDk.notFoundSku();
                    } else {
                        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                .setSkuDetails(skuDetail)
                                .setObfuscatedAccountId(tmpTransaction)
                                .setObfuscatedProfileId(tmpTransaction)
                                .build();
                        billingClient.launchBillingFlow(context, billingFlowParams);
                    }
                } else {
                    listenerSDk.onFailed(message);
                }
            }

            @Override
            public <T> void onupdatedTransaction(boolean success, String message, T data, boolean paymentSuccess) {
                if (success) {
                    listenerSDk.onSuccess();
                } else {
                    if (paymentSuccess)
                        listenerSDk.onFailed(message);
                }
            }

            @Override
            public void onGetSuccess(boolean success, String message, List<Products> data) {
//                if (success) {
//                    if (data != null && data.size() > 0) {
//                        skuList = new ArrayList<>();
//                        for (Products productData : data) {
//                            skuList.add(productData.getId());
//                        }
//                        isLoadedListProduct = true;
//                        initializingBillingClient();
//                    }
//                }
            }
        });
        initializingBillingClient();
    }

    public void initializingListProduct() {
//        presenter.loadListProduct();
        loadProductList();
    }

    public void initializingBillingClient() {
        Log.d(TAG, "Sku List: " + JsonHelper.toJson(skuList));
        billingClient = BillingClient.newBuilder(context)
                .setListener((billingResult, purchases) -> {
                    //TODO: Hàm này sẽ trả về kết quả khi người dùng thực hiện mua hàng.
                    /*
                    Đây là 1 số mã code trả về khi thanh toán
                    SERVICE_TIMEOUT = -3;
                    FEATURE_NOT_SUPPORTED = -2;
                    SERVICE_DISCONNECTED = -1;
                    OK = 0;
                    USER_CANCELED = 1;
                    SERVICE_UNAVAILABLE = 2;
                    BILLING_UNAVAILABLE = 3;
                    ITEM_UNAVAILABLE = 4;
                    DEVELOPER_ERROR = 5;
                    ERROR = 6;
                    ITEM_ALREADY_OWNED = 7;
                    ITEM_NOT_OWNED = 8;
                     */
                    Log.d(TAG, "initializingBillingClient: code: " + billingResult.getResponseCode());
                    Log.d(TAG, "initializingBillingClient: massage: " + billingResult.getDebugMessage());
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && purchases != null) {
                        for (Purchase purchase : purchases) {
                            handlePurchase(purchase);
                        }
                    } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                        // Handle an error caused by a user cancelling the purchase flow.
                        listenerSDk.onCancel();
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                presenter.updatedTransaction(tmpProductCode, null, false, String.valueOf(billingResult.getResponseCode()), "BillingResponseCode.USER_CANCELED", tmpTransaction);
                            }
                        });
                    } else {
                        // Handle any other error codes.
                        listenerSDk.onFailed("Error buy product with code: " + billingResult.getResponseCode());
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                presenter.updatedTransaction(tmpProductCode, null, false, String.valueOf(billingResult.getResponseCode()), billingResult.getDebugMessage(), tmpTransaction);
                            }
                        });
                    }
                })
                .enablePendingPurchases()
                .build();

        //TODO: Connect ứng dụng với Google Billing
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
                //TODO: Sau khi connect thàn h công, thử lấy thông tin các sản phẩm
                //Nếu response thành công thì bắt đầu thực hiện lấy về sản phẩm
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Log.d(TAG, "onBillingSetupFinished: Connect OK");
                    tryQueryProductsList(skuList);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                //TODO: Connect Google Play not success
                Log.d(TAG, "onBillingServiceDisconnected: Connect Google Play not success");
            }
        });
    }

    public void tryQueryProductsList(List<String> skuList) {
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(),
                (billingResult, skuDetailsList) -> {
                    Log.d(TAG, "onSkuDetailsResponse: response result code: " + billingResult.getResponseCode());
                    Log.d(TAG, "onSkuDetailsResponse: response result message: " + billingResult.getDebugMessage());
                    Log.d(TAG, "onSkuDetailsResponse: response OK" + skuDetailsList);
                    if (skuDetailsList != null) {
                        skuDetails.addAll(skuDetailsList);//lấy list ra để sử dụng
                    }
                });
    }

    public void loadProductList() {
        skuList = new ArrayList<>();
        skuList.add("99cent_buy_gold");
        skuList.add("4699cent_buy_gold");

    }

    public void buyProduct(String serverId, String gameClientTransId, String userId, String productCode, IAPPaymentResult listener) {
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                IAPUtils.this.listenerSDk = listener;
                tmpProductCode = productCode;
                presenter.createdTransaction(productCode, userId, gameClientTransId, serverId);
            }
        });
    }

    void handlePurchase(Purchase purchase) {
        ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();

        ConsumeResponseListener listener = (billingResult, purchaseToken) -> {
            Log.d(TAG, "handlePurchase: " + JsonHelper.toJson(purchase));
//                listenerSDk.onSuccess();
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    presenter.updatedTransaction(tmpProductCode, purchaseToken, true, String.valueOf(billingResult.getResponseCode()), billingResult.getDebugMessage(), tmpTransaction);
                }
            });
        };
        billingClient.consumeAsync(consumeParams, listener);
    }
}
