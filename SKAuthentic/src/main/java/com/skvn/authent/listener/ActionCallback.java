package com.skvn.authent.listener;

import com.skvn.authent.model.base.CallbackResponse;

public interface ActionCallback {
    void onCompleteAction(CallbackResponse response);
}
