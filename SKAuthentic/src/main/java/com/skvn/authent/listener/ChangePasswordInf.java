package com.skvn.authent.listener;

import com.skvn.authent.model.res.ResReqForgot;

public interface ChangePasswordInf {
    void inputError(boolean b,String errorPass,String errorConfirm);

    void setPassSuccess(boolean b,String message);

    void onPreload();

    void onSuccess();

    void verifyEmailSuccess(boolean success, String message, ResReqForgot data);

    void resendOtp(boolean success, String message, String data);

    void verifyOtp(boolean success, String message);

    void resetPassword(boolean success, String message);
}
