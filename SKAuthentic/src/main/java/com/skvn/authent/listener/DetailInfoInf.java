package com.skvn.authent.listener;

import com.skvn.authent.model.res.UserInfo;

public interface DetailInfoInf {
    void getInfoSuccess(boolean b, UserInfo userInfo);
}
