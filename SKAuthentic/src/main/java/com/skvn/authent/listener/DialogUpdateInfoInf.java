package com.skvn.authent.listener;

public interface DialogUpdateInfoInf {
    void inputError(boolean b, String input);

    void verifyEmailSuccess(boolean success, String message, String otpSession, boolean oldEmail);

    void verifyOtp(boolean success,String message, boolean isOld);

    void resendOtp(boolean success, String message, String data);
}
