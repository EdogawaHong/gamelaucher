package com.skvn.authent.listener;

public interface LoginInf {
    void inputError(boolean success,String errorName,String errorPass, String user_name, String pass);

    void loginSuccess(boolean success, String userName);

    void getInfoSuccess(boolean success, String userName);
}

