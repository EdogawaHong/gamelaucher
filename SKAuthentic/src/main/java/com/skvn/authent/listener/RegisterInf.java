package com.skvn.authent.listener;

import com.skvn.authent.model.req.RegisterData;

public interface RegisterInf {
    void inputError(boolean b,String errorName,String errorPass,String errorConfirm);
    void registerSuccess(boolean success, String message, RegisterData  registerData);
}
