package com.skvn.authent.listener;

public interface SetEmailInf {
    void inputError(boolean b,String email);

    void setEmailSuccess(boolean success,String message,String otpSession);
    void resendOtp(boolean success, String message, String otpSession);

    void verifyOtp(boolean b,String message);
}


