package com.skvn.authent.listener;

public interface SkyGameSdkInf {
    void checkSessionSuccess(boolean success);

    void getInfoSuccess(boolean success, String user_name);
}
