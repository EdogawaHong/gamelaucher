package com.skvn.authent.listener;

public interface UpdateInfoInf {
    void inputError(boolean b, String errorName, String errorBirthday, String errorCmnd);

    void updateSuccess(boolean success, String message);
}
