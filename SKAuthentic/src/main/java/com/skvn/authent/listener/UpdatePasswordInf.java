package com.skvn.authent.listener;

public interface UpdatePasswordInf {
    public void inputError(boolean b, String errorOld, String errorNew, String errorConfirm);

    void changePassSuccess(boolean success, String message);
}
