package com.skvn.authent.model.data;

import com.google.gson.annotations.Expose;

public class DeviceInfo {
    @Expose
    private String code_name;
    @Expose
    private String release;
    @Expose
    private String incremental;
    @Expose
    private int sdk_int;
    @Expose
    private String device;

    public DeviceInfo(String code_name, String release, String incremental, int sdk_int, String device) {
        this.code_name = code_name;
        this.release = release;
        this.incremental = incremental;
        this.sdk_int = sdk_int;
        this.device = device;
    }

    public String getCode_name() {
        return code_name;
    }

    public void setCode_name(String code_name) {
        this.code_name = code_name;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getIncremental() {
        return incremental;
    }

    public void setIncremental(String incremental) {
        this.incremental = incremental;
    }

    public int getSdk_int() {
        return sdk_int;
    }

    public void setSdk_int(int sdk_int) {
        this.sdk_int = sdk_int;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "DeviceInfo: " + "code_name=" + code_name + ", release=" + release + ", incremental=" + incremental + ", sdk_int=" + sdk_int + ", device='" + device + '\'';
    }
}
