package com.skvn.authent.model.req;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

    @SerializedName("gameId")
    @Expose
    private String gameId;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("appKey")
    @Expose
    private String appKey;
    @SerializedName("deviceInfo")
    @Expose
    private String deviceInfo;

    public Login(String gameId, String userName, String password, String appKey, String deviceInfo) {
        this.gameId = gameId;
        this.userName = userName;
        this.password = password;
        this.appKey = appKey;
        this.deviceInfo = deviceInfo;
    }

    public Login() {
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

}