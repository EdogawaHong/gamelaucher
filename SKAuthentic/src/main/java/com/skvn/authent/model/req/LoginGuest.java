package com.skvn.authent.model.req;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginGuest {

    @SerializedName("gameId")
    @Expose
    private String gameId;
    @SerializedName("guestId")
    @Expose
    private String guestId;
    @SerializedName("appKey")
    @Expose
    private String appKey;
    @SerializedName("deviceInfo")
    @Expose
    private String deviceInfo;

    public LoginGuest(String gameId, String guestId, String appKey, String deviceInfo) {
        this.gameId = gameId;
        this.guestId = guestId;
        this.appKey = appKey;
        this.deviceInfo = deviceInfo;
    }

    public LoginGuest() {
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGuestId() {
        return guestId;
    }

    public void setGuestId(String guestId) {
        this.guestId = guestId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

}