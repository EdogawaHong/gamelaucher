package com.skvn.authent.model.req;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Logout {

    @SerializedName("gameId")
    @Expose
    private String gameId;
    @SerializedName("session")
    @Expose
    private String session;
    @SerializedName("cgUuid")
    @Expose
    private String cgUuid;

    public Logout() {
    }

    public Logout(String gameId, String session, String cgUuid) {
        this.gameId = gameId;
        this.session = session;
        this.cgUuid = cgUuid;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
}
