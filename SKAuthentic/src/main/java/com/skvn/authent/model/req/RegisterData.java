package com.skvn.authent.model.req;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterData implements Serializable {

@SerializedName("gameId")
@Expose
private Integer gameId;
@SerializedName("userName")
@Expose
private String userName;
@SerializedName("password")
@Expose
private String password;

public Integer getGameId() {
return gameId;
}

public void setGameId(Integer gameId) {
this.gameId = gameId;
}

public String getUserName() {
return userName;
}

public void setUserName(String userName) {
this.userName = userName;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

}