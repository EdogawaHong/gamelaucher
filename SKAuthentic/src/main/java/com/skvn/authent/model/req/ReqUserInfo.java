package com.skvn.authent.model.req;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReqUserInfo {

    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("cmndNumber")
    @Expose
    private String cmndNumber;
    @SerializedName("cmndCreateDate")
    @Expose
    private String cmndCreateDate;
    @SerializedName("cmndCreatePlace")
    @Expose
    private String cmndCreatePlace;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("deviceInfo")
    @Expose
    private String deviceInfo;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCmndNumber() {
        return cmndNumber;
    }

    public void setCmndNumber(String cmndNumber) {
        this.cmndNumber = cmndNumber;
    }

    public String getCmndCreateDate() {
        return cmndCreateDate;
    }

    public void setCmndCreateDate(String cmndCreateDate) {
        this.cmndCreateDate = cmndCreateDate;
    }

    public String getCmndCreatePlace() {
        return cmndCreatePlace;
    }

    public void setCmndCreatePlace(String cmndCreatePlace) {
        this.cmndCreatePlace = cmndCreatePlace;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

}
