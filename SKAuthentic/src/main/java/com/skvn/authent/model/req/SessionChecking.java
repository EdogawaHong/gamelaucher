package com.skvn.authent.model.req;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionChecking {

@SerializedName("gameId")
@Expose
private String gameId;
@SerializedName("session")
@Expose
private String session;

    public SessionChecking() {
    }

    public SessionChecking(String gameId, String session) {
        this.gameId = gameId;
        this.session = session;
    }

    public String getGameId() {
return gameId;
}

public void setGameId(String gameId) {
this.gameId = gameId;
}

public String getSession() {
return session;
}

public void setSession(String session) {
this.session = session;
}

}