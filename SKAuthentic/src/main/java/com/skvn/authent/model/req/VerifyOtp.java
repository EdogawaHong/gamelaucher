package com.skvn.authent.model.req;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyOtp {

    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("otpSession")
    @Expose
    private String otpSession;
    @SerializedName("deviceInfo")
    @Expose
    private String deviceInfo;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpSession() {
        return otpSession;
    }

    public void setOtpSession(String otpSession) {
        this.otpSession = otpSession;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

}

