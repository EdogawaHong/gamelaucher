package com.skvn.authent.model.req.iap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateTransaction {

    @SerializedName("paymentChannel")
    @Expose
    private String paymentChannel;
    @SerializedName("serverId")
    @Expose
    private String serverId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("deviceInfo")
    @Expose
    private String deviceInfo;
    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("productId")
    @Expose
    private String productId;
    @SerializedName("gameClientTransId")
    @Expose
    private String gameClientTransId;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getGameClientTransId() {
        return gameClientTransId;
    }

    public void setGameClientTransId(String gameClientTransId) {
        this.gameClientTransId = gameClientTransId;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(String deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}