package com.skvn.authent.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginData {

    @SerializedName("gameId")
    @Expose
    private String gameId;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("session")
    @Expose
    private String session;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("cgUuid")
    @Expose
    private String cgUuid;
    @SerializedName("accessToken")
    @Expose
    private String accessToken;

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCgUuid() {
        return cgUuid;
    }

    public void setCgUuid(String cgUuid) {
        this.cgUuid = cgUuid;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

}