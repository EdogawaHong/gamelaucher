package com.skvn.authent.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResReqForgot {
    @SerializedName("session")
    @Expose
    private String session;
    @SerializedName("custId")
    @Expose
    private String custId;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
}
