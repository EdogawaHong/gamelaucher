package com.skvn.authent.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionCheckingResult {

@SerializedName("statusOnline")
@Expose
private Integer statusOnline;

public Integer getStatusOnline() {
return statusOnline;
}

public void setStatusOnline(Integer statusOnline) {
this.statusOnline = statusOnline;
}

}