package com.skvn.authent.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserInfo implements Serializable {

@SerializedName("id")
@Expose
private String id;
@SerializedName("avatar")
@Expose
private String avatar;
@SerializedName("fullName")
@Expose
private String fullName;
@SerializedName("email")
@Expose
private String email;
@SerializedName("phone")
@Expose
private String phone;
@SerializedName("cmndNumber")
@Expose
private String cmndNumber;
@SerializedName("cmndCreateDate")
@Expose
private String cmndCreateDate;
@SerializedName("cmndCreatePlace")
@Expose
private String cmndCreatePlace;
@SerializedName("status")
@Expose
private Integer status;
@SerializedName("birthday")
@Expose
private String birthday;
@SerializedName("address")
@Expose
private String address;
@SerializedName("code")
@Expose
private String code;
@SerializedName("account")
@Expose
private String account;
@SerializedName("password")
@Expose
private String password;
@SerializedName("passwordStatus")
@Expose
private Integer passwordStatus;
@SerializedName("accountStatus")
@Expose
private Integer accountStatus;
@SerializedName("channel")
@Expose
private String channel;
@SerializedName("emailStatus")
@Expose
private Integer emailStatus;
@SerializedName("phoneStatus")
@Expose
private Integer phoneStatus;
@SerializedName("createTime")
@Expose
private String createTime;
@SerializedName("createBy")
@Expose
private String createBy;
@SerializedName("updateTime")
@Expose
private String updateTime;
@SerializedName("updateBy")
@Expose
private String updateBy;

public String getId() {
return id;
}

public void setId(String id) {
this.id = id;
}

public String getAvatar() {
return avatar;
}

public void setAvatar(String avatar) {
this.avatar = avatar;
}

public String getFullName() {
return fullName;
}

public void setFullName(String fullName) {
this.fullName = fullName;
}

public String getEmail() {
return email;
}

public void setEmail(String email) {
this.email = email;
}

public String getPhone() {
return phone;
}

public void setPhone(String phone) {
this.phone = phone;
}

public String getCmndNumber() {
return cmndNumber;
}

public void setCmndNumber(String cmndNumber) {
this.cmndNumber = cmndNumber;
}

public String getCmndCreateDate() {
return cmndCreateDate;
}

public void setCmndCreateDate(String cmndCreateDate) {
this.cmndCreateDate = cmndCreateDate;
}

public String getCmndCreatePlace() {
return cmndCreatePlace;
}

public void setCmndCreatePlace(String cmndCreatePlace) {
this.cmndCreatePlace = cmndCreatePlace;
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public String getBirthday() {
return birthday;
}

public void setBirthday(String birthday) {
this.birthday = birthday;
}

public String getAddress() {
return address;
}

public void setAddress(String address) {
this.address = address;
}

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

public String getAccount() {
return account;
}

public void setAccount(String account) {
this.account = account;
}

public String getPassword() {
return password;
}

public void setPassword(String password) {
this.password = password;
}

public Integer getPasswordStatus() {
return passwordStatus;
}

public void setPasswordStatus(Integer passwordStatus) {
this.passwordStatus = passwordStatus;
}

public Integer getAccountStatus() {
return accountStatus;
}

public void setAccountStatus(Integer accountStatus) {
this.accountStatus = accountStatus;
}

public String getChannel() {
return channel;
}

public void setChannel(String channel) {
this.channel = channel;
}

public Integer getEmailStatus() {
return emailStatus;
}

public void setEmailStatus(Integer emailStatus) {
this.emailStatus = emailStatus;
}

public Integer getPhoneStatus() {
return phoneStatus;
}

public void setPhoneStatus(Integer phoneStatus) {
this.phoneStatus = phoneStatus;
}

public String getCreateTime() {
return createTime;
}

public void setCreateTime(String createTime) {
this.createTime = createTime;
}

public String getCreateBy() {
return createBy;
}

public void setCreateBy(String createBy) {
this.createBy = createBy;
}

public String getUpdateTime() {
return updateTime;
}

public void setUpdateTime(String updateTime) {
this.updateTime = updateTime;
}

public String getUpdateBy() {
return updateBy;
}

public void setUpdateBy(String updateBy) {
this.updateBy = updateBy;
}

}