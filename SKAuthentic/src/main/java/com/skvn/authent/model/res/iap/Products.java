package com.skvn.authent.model.res.iap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Products {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("priceRmb")
    @Expose
    private String priceRmb;
    @SerializedName("priceVnd")
    @Expose
    private String priceVnd;
    @SerializedName("priceUsd")
    @Expose
    private String priceUsd;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriceRmb() {
        return priceRmb;
    }

    public void setPriceRmb(String priceRmb) {
        this.priceRmb = priceRmb;
    }

    public String getPriceVnd() {
        return priceVnd;
    }

    public void setPriceVnd(String priceVnd) {
        this.priceVnd = priceVnd;
    }

    public String getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(String priceUsd) {
        this.priceUsd = priceUsd;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}