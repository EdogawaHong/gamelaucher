package com.skvn.authent.presenter;

import android.content.Context;

import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.ChangePasswordInf;
import com.skvn.authent.listener.DialogUpdateInfoInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.ForgotPassword;
import com.skvn.authent.model.req.ResetPassword;
import com.skvn.authent.model.req.VerifyOtp;
import com.skvn.authent.model.res.ResReqForgot;
import com.skvn.authent.view.dialog.DialogListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassPresent {

    Context context;
    ChangePasswordInf inf;
    APIService apiService;

    public ChangePassPresent(Context context, ChangePasswordInf inf) {
        this.context = context;
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
    }

    public void sendEmailOtp(String emailOrUserName) {
        inf.onPreload();
        ForgotPassword forgotPassword=new ForgotPassword();
        forgotPassword.setDeviceInfo(Utils.getUUID(context));
        forgotPassword.setUserName(emailOrUserName);
        apiService.forgotPass(forgotPassword).enqueue(new Callback<BaseResponse<ResReqForgot>>() {
            @Override
            public void onResponse(Call<BaseResponse<ResReqForgot>> call, Response<BaseResponse<ResReqForgot>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.verifyEmailSuccess(true, response.body().getError().getMessage(),response.body().getData());
                    inf.onSuccess();
                } else {
                    inf.verifyEmailSuccess(false,response.body().getError().getMessage(),null);
                    inf.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<ResReqForgot>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.verifyEmailSuccess(false,t.getMessage(),null);
                inf.onSuccess();
            }
        });
    }

    public void verifyOtp(String otp,String otpSession, String cusId){
        inf.onPreload();
//        String authorization = ApiUtils.getHeaderAuthorization(context);
        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setOtp(otp);
        verifyOtp.setOtpSession(otpSession);
        verifyOtp.setDeviceInfo(Utils.getUUID(context));
        apiService.verifyOtpPass("",verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.verifyOtp(true,response.body().getError().getMessage());
                    inf.onSuccess();
                } else {
                    inf.verifyOtp(false,response.body().getError().getMessage());
                    inf.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                inf.verifyOtp(false,t.getMessage());
                inf.onSuccess();
            }
        });
    }

    public void resetPassword(String newPass,String otpSession){
        inf.onPreload();
        String authorization = null;
        ResetPassword reset=new ResetPassword();
        reset.setNewPass(newPass);
        reset.setOtpSession(otpSession);
        reset.setDeviceInfo(Utils.getUUID(context));
        apiService.resetPassword("", reset).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.resetPassword(true,response.body().getError().getMessage());
                    inf.onSuccess();
                } else {
                    inf.resetPassword(false,response.body().getError().getMessage());
                    inf.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                inf.resetPassword(false,t.getMessage());
                inf.onSuccess();
            }
        });
    }

    public void resendOtp(String sessionOtp) {
        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setOtpSession(sessionOtp);
        verifyOtp.setDeviceInfo(Utils.getUUID(context));
        inf.onPreload();
        String authorization = null;
        apiService.resendOtpForgotPass("", verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.resendOtp(true,response.body().getError().getMessage(),response.body().getData());
                    inf.onSuccess();
                } else {
                    inf.resendOtp(false,response.body().getError().getMessage(),"");
                    inf.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.resendOtp(false,t.getMessage(),"");
                inf.onSuccess();
            }
        });
    }
}
