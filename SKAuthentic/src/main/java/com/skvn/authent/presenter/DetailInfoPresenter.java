package com.skvn.authent.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.DetailInfoInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.view.dialog.DialogListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailInfoPresenter {
    Context context;
    DetailInfoInf inf;
    DialogListener listener;
    APIService apiService;

    public DetailInfoPresenter(Context context, DetailInfoInf inf, DialogListener listener) {
        this.context = context;
        this.inf = inf;
        this.listener = listener;
        apiService = ApiUtils.getAPIService();
    }

    public void getInfo(){
        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.getUserInfo(authorization).enqueue(new Callback<BaseResponse<UserInfo>>() {
            @Override
            public void onResponse(Call<BaseResponse<UserInfo>> call, Response<BaseResponse<UserInfo>> response) {
                if(ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_INFO, new Gson().toJson(response.body().getData()));
                    inf.getInfoSuccess(true,response.body().getData());
                    listener.onSuccess();
                } else{
                    inf.getInfoSuccess(false,null);
                    listener.onSuccess();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<UserInfo>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.getInfoSuccess(false,null);
                listener.onSuccess();
            }
        });
    }
}
