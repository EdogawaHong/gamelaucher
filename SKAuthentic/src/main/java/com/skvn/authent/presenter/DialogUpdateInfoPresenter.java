package com.skvn.authent.presenter;

import android.content.Context;

import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.DialogUpdateInfoInf;
import com.skvn.authent.listener.SetEmailInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.VerifyOtp;
import com.skvn.authent.view.dialog.DialogListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogUpdateInfoPresenter {
    Context context;
    DialogListener listener;
    DialogUpdateInfoInf inf;
    APIService apiService;

    public DialogUpdateInfoPresenter(Context context, DialogListener listener, DialogUpdateInfoInf inf) {
        this.context = context;
        this.listener = listener;
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
    }

    public void sendOtpOldEmail(String email){
        listener.onPreload();
        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setDeviceInfo(Utils.getUUID(context));
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.sendOtpOldEmailChange(authorization,email, verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.verifyEmailSuccess(true,response.body().getError().getMessage(),response.body().getData(), true);
                    listener.onSuccess();
                } else {
                    inf.verifyEmailSuccess(false,response.body().getError().getMessage(),"", true);
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.verifyEmailSuccess(false,t.getMessage(),"", true);
                listener.onSuccess();
            }
        });
    }
    public void sendOtpNewEmail(String email, String sessionOtp){
        listener.onPreload();
        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setOtpSession(sessionOtp);
        verifyOtp.setDeviceInfo(Utils.getUUID(context));
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.sendOtpNewEmailChange(authorization,email, verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.verifyEmailSuccess(true,response.body().getError().getMessage(),response.body().getData(), false);
                    listener.onSuccess();
                } else {
                    inf.verifyEmailSuccess(false,response.body().getError().getMessage(),"", false);
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.verifyEmailSuccess(false,t.getMessage(),"", false);
                listener.onSuccess();
            }
        });
    }

    public void resendOtp(String sessionOtp){

        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setOtpSession(sessionOtp);
        verifyOtp.setDeviceInfo(Utils.getUUID(context));

        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.resendOtp(authorization,verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.resendOtp(true,response.body().getError().getMessage(),response.body().getData());
                    listener.onSuccess();
                } else {
                    inf.resendOtp(false,response.body().getError().getMessage(),"");
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.resendOtp(false,t.getMessage(),"");
                listener.onSuccess();
            }
        });
    }

    public void verifyOtp(String otp,String otpSession, boolean isOld){
        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setOtp(otp);
        verifyOtp.setOtpSession(otpSession);
        verifyOtp.setDeviceInfo(Utils.getUUID(context));
        apiService.verifyOtp(authorization,verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.verifyOtp(true,response.body().getError().getMessage(), isOld);
                    listener.onSuccess();
                } else {
                    inf.verifyOtp(false,response.body().getError().getMessage(), isOld);
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                inf.verifyOtp(false,t.getMessage(), isOld);
                listener.onSuccess();
            }
        });
    }

//    public void checkData(int type,String data){
//        if(type==1){
//            if(data.length()!=10 || !"0".equals(data.charAt(0))){
//                inf.inputError(false,"Số điện thoại không hợp lệ");
//            }else{
//                inf.inputError(true,"");
//            }
//        }else if(type==2){
//            Pattern pattern = Pattern.compile("^.+@.+\\..+$");
//            Matcher matcher = pattern.matcher(data);
//            if(matcher.matches()){
//                inf.inputError(true,"");
//            }else{
//                inf.inputError(false,"Số điện thoại không hợp lệ");
//            }
//        }
//    }
}
