package com.skvn.authent.presenter;

import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.model.stackover.SOAnswersResponse;
import com.skvn.authent.view.home.HomeInf;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivityPresenter {

    HomeInf view;
    APIService apiService;

    public HomeActivityPresenter(HomeInf view) {
        this.view = view;
        apiService = ApiUtils.getAPIService();
    }

    void onLogin(String user_name, String password){
        apiService.getAnswers().enqueue(new Callback<List<SOAnswersResponse>>() {
            @Override
            public void onResponse(Call<List<SOAnswersResponse>> call, Response<List<SOAnswersResponse>> response) {

            }

            @Override
            public void onFailure(Call<List<SOAnswersResponse>> call, Throwable t) {

            }
        });
    }
}
