package com.skvn.authent.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.skvn.authent.R;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.JsonHelper;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.LoginInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.data.DeviceInfo;
import com.skvn.authent.model.req.Login;
import com.skvn.authent.model.req.LoginGuest;
import com.skvn.authent.model.res.LoginData;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.view.dialog.DialogListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {
    Context context;
    LoginInf inf;
    DialogListener listener;
    APIService apiService;

    public LoginPresenter(Context context, LoginInf inf, DialogListener listener) {
        this.context = context;
        this.inf = inf;
        this.listener = listener;
        apiService = ApiUtils.getAPIService();
    }

    public void checkInfo(String name, String pass) {
        String errorName = "", errorPass = "";
        if (pass.equals("") || pass == null) {
            errorPass = context.getResources().getString(R.string.not_pass);
        } else {
            if (pass.length() < 6)
                errorPass = context.getResources().getString(R.string.not_lenght_password);
        }
        if (name.equals("") || name == null) {
            errorName = context.getResources().getString(R.string.not_name);
        }

        if ((errorPass == null || errorPass.equals("")) &&
                (errorName == null || errorName.equals(""))) {
            inf.inputError(true, null, null, name, pass);
        } else {
            inf.inputError(false, errorName, errorPass, null, null);
        }
    }

    public void login(Context context, String user_name, String pass_word) {
        DeviceInfo info = Utils.getDeviceInfo();
        String json_device_data = JsonHelper.toJson(info);
        Login login = new Login(AppConstance.GAME_ID, user_name, pass_word, "android", json_device_data);

        apiService.loginSKW(login).enqueue(new Callback<BaseResponse<LoginData>>() {
            @Override
            public void onResponse(Call<BaseResponse<LoginData>> call, Response<BaseResponse<LoginData>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_DATA, new Gson().toJson(response.body().getData()));
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_NAME, response.body().getData().getUserName());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.TOKEN, response.body().getData().getAccessToken());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.SESSION, response.body().getData().getSession());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.CGUUID, response.body().getData().getCgUuid());
                    inf.loginSuccess(true, response.body().getData().getUserName());
                } else {
                    inf.loginSuccess(false, null);
                    Utils.showToast(context, response.body().getError().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<LoginData>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.loginSuccess(false, null);
            }
        });
    }

    public void loginGoogle(Context context, String google_id_token) {
        DeviceInfo info = Utils.getDeviceInfo();
        String json_device_data = JsonHelper.toJson(info);
        apiService.loginGoogle(google_id_token, AppConstance.GAME_ID, "android", Utils.getUUID(context)).enqueue(new Callback<BaseResponse<LoginData>>() {
            @Override
            public void onResponse(Call<BaseResponse<LoginData>> call, Response<BaseResponse<LoginData>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_DATA, new Gson().toJson(response.body().getData()));
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_NAME, response.body().getData().getUserName());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.TOKEN, response.body().getData().getAccessToken());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.SESSION, response.body().getData().getSession());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.CGUUID, response.body().getData().getCgUuid());
                    inf.loginSuccess(true, response.body().getData().getUserName());
                } else {
                    inf.loginSuccess(false, null);
                    Utils.showToast(context, response.body().getError().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<LoginData>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.loginSuccess(false, null);
            }
        });
    }

    public void loginFacebook(Context context, String facebook_id_token) {
        DeviceInfo info = Utils.getDeviceInfo();
        String json_device_data = JsonHelper.toJson(info);
        apiService.loginFacebook(facebook_id_token, AppConstance.GAME_ID, "android", Utils.getUUID(context)).enqueue(new Callback<BaseResponse<LoginData>>() {
            @Override
            public void onResponse(Call<BaseResponse<LoginData>> call, Response<BaseResponse<LoginData>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_DATA, new Gson().toJson(response.body().getData()));
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_NAME, response.body().getData().getUserName());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.TOKEN, response.body().getData().getAccessToken());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.SESSION, response.body().getData().getSession());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.CGUUID, response.body().getData().getCgUuid());
                    inf.loginSuccess(true, response.body().getData().getUserName());
                } else {
                    inf.loginSuccess(false, null);
                    Utils.showToast(context, response.body().getError().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<LoginData>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.loginSuccess(false, null);
            }
        });
    }

    public void getInfo() {
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.getUserInfo(authorization).enqueue(new Callback<BaseResponse<UserInfo>>() {
            @Override
            public void onResponse(Call<BaseResponse<UserInfo>> call, Response<BaseResponse<UserInfo>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_INFO, new Gson().toJson(response.body().getData()));
                    inf.getInfoSuccess(true, null);
                } else {
                    inf.getInfoSuccess(false, null);
//                    Utils.showToast(context, response.body().getError().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<UserInfo>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.getInfoSuccess(false, null);
            }
        });
    }

    public void loginSKW(Context context, String gameId, String user_name, String pass_word) {


    }

    public void loginGuest(Context context) {
        DeviceInfo info = Utils.getDeviceInfo();
        String json_device_data = JsonHelper.toJson(info);
        LoginGuest loginGuest = new LoginGuest(AppConstance.GAME_ID, Utils.getUUID(context), "android", json_device_data);

        apiService.loginGuest(loginGuest).enqueue(new Callback<BaseResponse<LoginData>>() {
            @Override
            public void onResponse(Call<BaseResponse<LoginData>> call, Response<BaseResponse<LoginData>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_DATA, new Gson().toJson(response.body().getData()));
                    SharedUtils.getInstance(context).putStringValue(AppConstance.TOKEN, response.body().getData().getAccessToken());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_NAME, response.body().getData().getUserName());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.SESSION, response.body().getData().getSession());
                    SharedUtils.getInstance(context).putStringValue(AppConstance.CGUUID, response.body().getData().getCgUuid());
                    inf.loginSuccess(true, response.body().getData().getUserName());
                } else {
                    inf.loginSuccess(false, null);
                    Utils.showToast(context, response.body().getError().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<LoginData>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.loginSuccess(false, null);
            }
        });
    }
}
