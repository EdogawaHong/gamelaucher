package com.skvn.authent.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.skvn.authent.R;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.LoginInf;
import com.skvn.authent.listener.RegisterInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.Login;
import com.skvn.authent.model.req.RegisterData;
import com.skvn.authent.model.res.LoginData;
import com.skvn.authent.view.dialog.DialogListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter {
    Context context;
    RegisterInf inf;
    DialogListener listener;
    APIService apiService;

    public RegisterPresenter(Context context, RegisterInf inf, DialogListener listener) {
        this.context = context;
        this.inf = inf;
        this.listener = listener;
        apiService = ApiUtils.getAPIService();
    }

    boolean checkSpecialCharacter(String inputString){
        Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
        Matcher matcher = pattern.matcher(inputString);
        return matcher.find();
    }

    public void checkInfo(String name, String pass, String confirmPass) {
        String errorName = "", errorPass = "", errorConfirm = "";
        if (name.equals("") || name == null) {
            errorName = context.getResources().getString(R.string.not_name);
        }

        if (checkSpecialCharacter(name)) {
            errorName = context.getResources().getString(R.string.not_valid_name);
        }

        if (pass.equals("") || pass == null) {
            errorPass = context.getResources().getString(R.string.not_pass);
        } else {
            if (pass.length() < 6)
                errorPass = context.getResources().getString(R.string.not_lenght_password);
        }

        if (confirmPass.equals("") || confirmPass == null) {
            errorConfirm = context.getResources().getString(R.string.not_confirm);
        } else {
            if (confirmPass.length() < 6)
                errorConfirm = context.getResources().getString(R.string.not_lenght_password);
        }
        if (pass.length() >= 6 && confirmPass.length() >= 6 && !pass.equals(confirmPass)) {
            errorConfirm = context.getResources().getString(R.string.not_match_password);
        }

        if ((errorPass == null || errorPass.equals("")) &&
                (errorConfirm == null || errorConfirm.equals("")) &&
                (errorName == null || errorName.equals(""))) {
            inf.inputError(true, null, null, null);
        } else {
            inf.inputError(false, errorName, errorPass, errorConfirm);
        }
    }

    public void register(String name, String pass) {
        RegisterData registerData = new RegisterData();
        registerData.setGameId(Integer.parseInt(AppConstance.GAME_ID));
        registerData.setUserName(name);
        registerData.setPassword(pass);

        apiService.register(registerData).enqueue(new Callback<BaseResponse<LoginData>>() {
            @Override
            public void onResponse(Call<BaseResponse<LoginData>> call, Response<BaseResponse<LoginData>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.registerSuccess(true, null, registerData);
                } else{
                    inf.registerSuccess(false, response.body().getError().getMessage(), registerData);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<LoginData>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.registerSuccess(false, null, registerData);
            }
        });
    }
}
