package com.skvn.authent.presenter;

import android.content.Context;

import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.SetEmailInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.SetPassData;
import com.skvn.authent.model.req.VerifyOtp;
import com.skvn.authent.view.dialog.DialogListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetEmailPresenter {
    Context context;
    DialogListener listener;
    SetEmailInf inf;
    APIService apiService;

    public SetEmailPresenter(Context context, DialogListener listener, SetEmailInf inf) {
        this.context = context;
        this.listener = listener;
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
    }

    public void setEmail(String email){
        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.sendOtpEmail(authorization,email).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.setEmailSuccess(true,response.body().getError().getMessage(),response.body().getData());
                    listener.onSuccess();
                } else {
                    inf.setEmailSuccess(false,response.body().getError().getMessage(),"");
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.setEmailSuccess(false,t.getMessage(),"");
                listener.onSuccess();
            }
        });
    }

    public void resendOtp(String sessionOtp){

        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setOtpSession(sessionOtp);
        verifyOtp.setDeviceInfo(Utils.getUUID(context));

        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.resendOtp(authorization,verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.resendOtp(true,response.body().getError().getMessage(),response.body().getData());
                    listener.onSuccess();
                } else {
                    inf.resendOtp(false,response.body().getError().getMessage(),"");
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.resendOtp(false,t.getMessage(),"");
                listener.onSuccess();
            }
        });
    }

    public void verifyOtp(String otp,String otpSession){
        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        VerifyOtp verifyOtp=new VerifyOtp();
        verifyOtp.setOtp(otp);
        verifyOtp.setOtpSession(otpSession);
        verifyOtp.setDeviceInfo(Utils.getUUID(context));
        apiService.verifyOtp(authorization,verifyOtp).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.verifyOtp(true,response.body().getError().getMessage());
                    listener.onSuccess();
                } else {
                    inf.verifyOtp(false,response.body().getError().getMessage());
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                inf.verifyOtp(false,t.getMessage());
                listener.onSuccess();
            }
        });
    }

    public void checkData(int type,String data){
        if(type==1){
            if(data.length()!=10 || !"0".equals(data.charAt(0))){
                inf.inputError(false,"Số điện thoại không hợp lệ");
            }else{
                inf.inputError(true,"");
            }
        }else if(type==2){
            Pattern pattern = Pattern.compile("^.+@.+\\..+$");
            Matcher matcher = pattern.matcher(data);
            if(matcher.matches()){
                inf.inputError(true,"");
            }else{
                inf.inputError(false,"Email không hợp lệ");
            }
        }
    }
}
