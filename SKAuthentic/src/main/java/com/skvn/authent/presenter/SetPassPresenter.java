package com.skvn.authent.presenter;

import android.content.Context;

import com.skvn.authent.R;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.SetPasswordInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.SetPassData;
import com.skvn.authent.view.dialog.DialogListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetPassPresenter {
    Context context;
    DialogListener listener;
    SetPasswordInf inf;
    APIService apiService;

    public SetPassPresenter(Context context, DialogListener listener, SetPasswordInf inf) {
        this.listener = listener;
        this.context = context;
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
    }

    public void setPassword(String pass) {
        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        String deviceInfo=Utils.getUUID(context);
        SetPassData req=new SetPassData();
        req.setPassword(pass);
        req.setDeviceInfo(deviceInfo);
        apiService.addPassword(authorization,req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.setPassSuccess(true,response.body().getError().getMessage());
                    listener.onSuccess();
                } else {
                    inf.setPassSuccess(false,response.body().getError().getMessage());
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                //Utils.showToast(context, t.getMessage());
                inf.setPassSuccess(false,t.getMessage());
                listener.onSuccess();
            }
        });
    }

    public void checkPassword(String pass, String confirm) {
        String errorPass = "", errorConfirm = "";
        if (pass.equals("") || pass == null) {
            errorPass =  context.getResources().getString(R.string.not_pass);
        } else {
            if (pass.length() < 6)
                errorPass =  context.getResources().getString(R.string.not_lenght_password);
        }
        if (confirm.equals("") || confirm == null) {
            errorConfirm = context.getResources().getString(R.string.not_confirm);
        } else {
            if (confirm.length() < 6)
                errorConfirm =context.getResources().getString(R.string.not_lenght_password);
        }
        if (pass.length() >= 6 && confirm.length() >= 6 && !pass.equals(confirm)) {
            errorConfirm = context.getResources().getString(R.string.not_match_password);
        }
        if ((errorPass == null || errorPass.equals("")) &&
                (errorConfirm == null || errorConfirm.equals(""))) {
            inf.inputError(true, null, null);

        } else {
            inf.inputError(false, errorPass, errorConfirm);
        }
    }
}
