package com.skvn.authent.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.JsonHelper;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.LoginInf;
import com.skvn.authent.listener.SkyGameSdkInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.data.DeviceInfo;
import com.skvn.authent.model.res.LoginData;
import com.skvn.authent.model.res.SessionCheckingResult;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.view.dialog.DialogListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SkyGameSdk {
    SkyGameSdkInf inf;
    Context context;
    APIService apiService;

    public SkyGameSdk(Context context, SkyGameSdkInf inf) {
        this.context = context;
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
    }

    public void getInfo() {
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.getUserInfo(authorization).enqueue(new Callback<BaseResponse<UserInfo>>() {
            @Override
            public void onResponse(Call<BaseResponse<UserInfo>> call, Response<BaseResponse<UserInfo>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_INFO, new Gson().toJson(response.body().getData()));
                    inf.getInfoSuccess(true, null);
                } else {
                    inf.getInfoSuccess(false, null);
//                    Utils.showToast(context, response.body().getError().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<UserInfo>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.getInfoSuccess(false, null);
            }
        });
    }


    public void checkSession(Context context) {
        String authorization = ApiUtils.getHeaderAuthorization(context);
        if (authorization == null) {
            authorization = "null";
        }
        apiService.checkSession(authorization, ApiUtils.BASE_VER_CODE_ANDROID, "android").enqueue(new Callback<BaseResponse<SessionCheckingResult>>() {
            @Override
            public void onResponse(Call<BaseResponse<SessionCheckingResult>> call, Response<BaseResponse<SessionCheckingResult>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    if (response.body().getData().getStatusOnline() == 1) {
                        inf.checkSessionSuccess(true);
                    } else {
                        inf.checkSessionSuccess(false);
                    }
                } else {
                    if (response.body().getError().getCode() != 333)
                        inf.checkSessionSuccess(false);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<SessionCheckingResult>> call, Throwable t) {
                inf.checkSessionSuccess(false);
            }
        });
    }
}
