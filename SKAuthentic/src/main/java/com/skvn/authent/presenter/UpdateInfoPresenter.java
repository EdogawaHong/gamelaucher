package com.skvn.authent.presenter;

import android.content.Context;

import com.skvn.authent.R;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.LoginInf;
import com.skvn.authent.listener.UpdateInfoInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.ChangePassData;
import com.skvn.authent.model.req.ReqUserInfo;
import com.skvn.authent.model.res.LoginData;
import com.skvn.authent.view.dialog.DialogListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateInfoPresenter {
    Context context;
    UpdateInfoInf inf;
    DialogListener listener;
    APIService apiService;

    public UpdateInfoPresenter(Context context, UpdateInfoInf inf, DialogListener listener) {
        this.context = context;
        this.inf = inf;
        this.listener = listener;
        apiService = ApiUtils.getAPIService();
    }

    public void checkInfo(String name, String birthday,String cmnd) {
        String errorName = "", errorBirthday = "",errorCmnd="";
        if (name.equals("") || name == null) {
            errorName = context.getResources().getString(R.string.not_fullname);
        }
        if (birthday.equals("") || birthday == null) {
            errorBirthday = context.getResources().getString(R.string.not_birthday);
        }if (cmnd.equals("") || cmnd == null) {
            errorCmnd = context.getResources().getString(R.string.not_cmnd);
        }

        if (cmnd.equals("") || cmnd == null) {
            errorCmnd = context.getResources().getString(R.string.not_cmnd);
        }
        if (cmnd.length() != 9 && cmnd.length() != 12 ) {
            errorCmnd = context.getResources().getString(R.string.not_valid_cmnd);
        }

        if ((errorName == null || errorName.equals("")) &&
                (errorBirthday == null || errorBirthday.equals("")) &&
                (errorCmnd == null || errorCmnd.equals(""))) {
            inf.inputError(true, null, null,null);
        } else {
            inf.inputError(false, errorName, errorBirthday,errorCmnd);
        }
    }

    public void updateInfo(ReqUserInfo req) {
        listener.onPreload();
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.updateInfo(authorization,req).enqueue(new Callback<BaseResponse<String>>() {
            @Override
            public void onResponse(Call<BaseResponse<String>> call, Response<BaseResponse<String>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    inf.updateSuccess(true, response.body().getError().getMessage());
                    listener.onSuccess();
                } else {
                    inf.updateSuccess(false, response.body().getError().getMessage());
                    listener.onSuccess();
                }
            }
            @Override
            public void onFailure(Call<BaseResponse<String>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.updateSuccess(false, t.getMessage());
                listener.onSuccess();
            }
        });
    }
}
