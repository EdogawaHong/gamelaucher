package com.skvn.authent.presenter;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.skvn.authent.R;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.UpdatePasswordInf;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.ChangePassData;
import com.skvn.authent.model.res.LoginData;
import com.skvn.authent.view.dialog.DialogListener;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePassPresenter {
    Context context;
    DialogListener listener;
    UpdatePasswordInf inf;
    APIService apiService;

    public UpdatePassPresenter(Context context, DialogListener listener, UpdatePasswordInf inf) {
        this.listener = listener;
        this.context = context;
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
    }

    public void updatePassword(String oldPass, String newPass, String confirmPass) {
        listener.onPreload();
        if (Utils.checkPassWord(context, newPass, confirmPass)) {
            ChangePassData changePass = new ChangePassData();
            changePass.setNewPassword(newPass);
            changePass.setOldPassword(oldPass);
            String authorization = ApiUtils.getHeaderAuthorization(context);
            apiService.changePass(changePass, authorization).enqueue(new Callback<BaseResponse<LoginData>>() {
                @Override
                public void onResponse(Call<BaseResponse<LoginData>> call, Response<BaseResponse<LoginData>> response) {
                    if (ApiUtils.notError(response.body().getError(), context)) {
                        inf.changePassSuccess(true, response.body().getError().getMessage());
                        listener.onSuccess();
                    } else{
                        inf.changePassSuccess(false, response.body().getError().getMessage());
                        listener.onSuccess();
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<LoginData>> call, Throwable t) {
                    //Utils.showToast(context, t.getMessage());
                    inf.changePassSuccess(false, t.getMessage());
                    listener.onSuccess();
                }
            });
        }
//        else {
//
//        }
//        new Timer().schedule(new TimerTask() {
//            @Override
//            public void run() {
//                listener.onSuccess();
//            }
//        }, 3000);
    }

    public void checkPassword(String oldPass, String newPass, String confirmPass) {
        String errorOld = "", errorNew = "", errorConfirm = "";
        if (oldPass.equals("") || oldPass == null) {
            errorOld = context.getResources().getString(R.string.not_old_pass);
        }
        if (newPass.equals("") || newPass == null) {
            errorNew = context.getResources().getString(R.string.not_new_pass);
        } else {
            if (newPass.length() < 6)
                errorNew = context.getResources().getString(R.string.not_lenght_password);
        }
        if (confirmPass.equals("") || confirmPass == null) {
            errorConfirm = context.getResources().getString(R.string.not_confirm);
        } else {
            if (confirmPass.length() < 6)
                errorConfirm = context.getResources().getString(R.string.not_lenght_password);
        }
        if (newPass.length() >= 6 && confirmPass.length() >= 6 && !newPass.equals(confirmPass)) {
            errorConfirm = context.getResources().getString(R.string.not_match_password);
        }
        if ((errorOld == null || errorOld.equals("")) &&
                (errorNew == null || errorNew.equals("")) &&
                (errorConfirm == null || errorConfirm.equals(""))) {
            inf.inputError(true, null, null, null);

        } else {
            inf.inputError(false, errorOld, errorNew, errorConfirm);

        }
    }
}
