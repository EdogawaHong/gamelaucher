package com.skvn.authent.view;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.CustomToast;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.ActionCallback;
import com.skvn.authent.listener.SkyGameSdkInf;
import com.skvn.authent.model.base.CallbackResponse;
import com.skvn.authent.presenter.SkyGameSdk;
import com.skvn.authent.view.headService.headinfo.HeadInfoActivity;
import com.skvn.authent.view.home.HomeActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SkyGameSdkAuthentActivity extends FragmentActivity implements SkyGameSdkInf {
    static Activity app;
    private static ActionCallback callback;
    static int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 9001;
    static int CODE_HANDLE_ACTION = 101;
    boolean isVerified = false;
    static boolean isLogout = false;

    boolean isClickStart = false;

    SkyGameSdk presenter;

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (android.content.pm.Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SkyGameSdk(getApplicationContext(), this);
        printKeyHash(this);
        Utils.hideSystemBar(this);
        app = this;
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("isClickStart")) {
                isClickStart = bundle.getBoolean("isClickStart");
            }
        }
        check();
        Log.e(TAG, "CALL TO SKYWAY SDK");
    }

    synchronized void check() {
        String session = SharedUtils.getInstance(app).getStringValue(AppConstance.TOKEN);
        if (session == null || session.length() == 0) {
            isVerified = false;
        } else {
            isVerified = true;
        }
        if (isVerified) {
            //Open the chat conversation click.
            if (!isClickStart) {
                Log.e(TAG, "SKYWAY SDK - USER INFOMATION");
                Intent intent = new Intent(this, HeadInfoActivity.class);
                startActivityForResult(intent, CODE_HANDLE_ACTION);
            } else {
                Log.e(TAG, "SKYWAY SDK - CHECK SESSION");
                presenter.checkSession(this);
            }
        } else {
            presenter.checkSession(this);
//            goToLogin();
        }
    }

    private void goToLogin() {
        Log.e(TAG, "SKYWAY SDK - LOGIN");
        Intent intent = new Intent(app, HomeActivity.class);
        startActivityForResult(intent, CODE_HANDLE_ACTION);
    }

    public static void setCompletedCallback(ActionCallback callback) {
        SkyGameSdkAuthentActivity.callback = callback;
    }

    private static final String TAG = "SkyGameSdkAuthentActivi";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CODE_HANDLE_ACTION) {
                if (data != null) {
                    CallbackResponse response = (CallbackResponse) data.getSerializableExtra(AppConstance.CALLBACK_RESPONSE);
                    if (response != null && callback != null) {
                        Log.e(TAG, "onActivityResult: " + response.getKey());
                        switch (response.getKey()) {
                            case AppConstance.ACTION_LOGIN_SUCCESS:
                                String token = SharedUtils.getInstance(SkyGameSdkAuthentActivity.this).getStringValue(AppConstance.TOKEN);
                                Log.e(TAG, "onActivityResult: " + AppConstance.ACTION_LOGIN_SUCCESS + " - " + token);
                                response.setValue(token);
                                callback.onCompleteAction(response);
                                finish();
                                break;
                            case AppConstance.ACTION_BACK:
                                if (response.getValue() != null && response.getValue().equals("login")) {
                                    callback.onCompleteAction(response);
                                }
                                finish();
                                break;
                            case AppConstance.ACTION_LOGOUT:
                                check();
                                callback.onCompleteAction(response);
                                break;
                        }
                    }
                } else{
                    finish();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "onActivityResult: ", e);
        }
    }

    @Override
    public void checkSessionSuccess(boolean success) {
        if (success){
            //Pass to Home or other screen
            presenter.getInfo();
        } else{
            Log.e(TAG, "SKYWAY SDK - CHECK SESSION - FAILED");
            //Logout User
            goToLogin();
        }
    }

    @Override
    public void getInfoSuccess(boolean success, String user_name) {
        Log.e(TAG, "SKYWAY SDK - CHECK SESSION - SUCCESS");
        String userName = SharedUtils.getInstance(app).getStringValue(AppConstance.USER_NAME);
        CustomToast.makeText(this, "Chào mừng " + userName + " đã đăng nhập!", CustomToast.LENGTH_SHORT, 1).show();
        finish();
    }
}
