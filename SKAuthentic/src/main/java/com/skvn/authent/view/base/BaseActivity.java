package com.skvn.authent.view.base;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.skvn.authent.common.Utils;
import com.skvn.authent.view.dialog.DialogLoading;

public class BaseActivity extends AppCompatActivity {
    Context context;
    DialogLoading loading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.hideSystemBar(this);
        context = this;
        loading = new DialogLoading(context);
    }

    public void showLoading(boolean show) {
        if (show) {
            loading.show();
        } else {
            loading.dismiss();
        }
    }
}
