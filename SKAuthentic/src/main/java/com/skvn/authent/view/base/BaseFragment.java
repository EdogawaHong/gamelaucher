package com.skvn.authent.view.base;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.skvn.authent.view.dialog.DialogLoading;

public class BaseFragment extends Fragment {
    Context context;
    DialogLoading loading;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = view.getContext();
        loading=new DialogLoading(context);
    }

    public void showLoading(boolean show){
        if (show) {
            loading.show();
        } else {
            loading.dismiss();
        }
    }
}
