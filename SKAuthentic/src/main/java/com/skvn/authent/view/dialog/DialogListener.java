package com.skvn.authent.view.dialog;

public interface DialogListener {
    void onPreload();
    void onSuccess();
    void onCancel();
}