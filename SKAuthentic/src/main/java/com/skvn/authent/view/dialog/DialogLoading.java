package com.skvn.authent.view.dialog;

import android.app.Dialog;
import android.content.Context;

import com.skvn.authent.R;

public class DialogLoading {
    Dialog progress;
    Context context;

    public DialogLoading(Context context) {
        this.context = context;
        this.progress = new Dialog(context);
        progress.setContentView(R.layout.progress_bar);
        progress.setCanceledOnTouchOutside(false);
    }
    public void show(){
        progress.show();
    }
    public void dismiss(){
        progress.dismiss();
    }
}
