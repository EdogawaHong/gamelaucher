package com.skvn.authent.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.GenericTextWatcher;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.SetEmailInf;
import com.skvn.authent.presenter.SetEmailPresenter;

public class DialogSetInfo implements SetEmailInf {
    Context context;
    DialogListener listener;
    SetEmailPresenter setEmailPresenter;

    public DialogSetInfo(Context context, DialogListener listener) {
        this.context = context;
        this.listener = listener;
        setEmailPresenter = new SetEmailPresenter(context, listener, this);
    }

    boolean isFirst = true;
    boolean sendCode = true, update = false;
    CountDownTimer countDownTimer = null;
    Dialog dialog;

    LinearLayout llOtp;
    Button btnUpdate;
    Button btnCancel;
    TextView sendOtp;
    TextView txtTime, txtErrorInput, txtError;
    EditText edtInfo;

    public void showDialogPhoneEmail(int type) {
        update = false;
        sendCode = true;
        countDownTimer = null;
        dialog = new Dialog(context, R.style.full_screen_dialog);
        dialog.getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_phone_email);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        edtInfo = dialog.findViewById(R.id.edtInfo);
        TextView txtTitle = dialog.findViewById(R.id.txtTitle);
        ImageView imgIcon = dialog.findViewById(R.id.imgIcon);

        sendOtp = dialog.findViewById(R.id.txtSendOtp);
        txtTime = dialog.findViewById(R.id.txtTime);

        llOtp = dialog.findViewById(R.id.llOtp);

        txtError = dialog.findViewById(R.id.txtError);
        txtErrorInput = dialog.findViewById(R.id.txtErrorInput);

        btnUpdate = dialog.findViewById(R.id.btnUpdate);
        btnCancel = dialog.findViewById(R.id.btnCancel);

        EditText otp1 = dialog.findViewById(R.id.otp1);
        EditText otp2 = dialog.findViewById(R.id.otp2);
        EditText otp3 = dialog.findViewById(R.id.otp3);
        EditText otp4 = dialog.findViewById(R.id.otp4);
        EditText otp5 = dialog.findViewById(R.id.otp5);
        EditText otp6 = dialog.findViewById(R.id.otp6);

        llOtp.setVisibility(View.GONE);
        txtError.setVisibility(View.GONE);

        btnUpdate.setBackground(context.getResources().getDrawable(R.drawable.disable_button_update));
        switch (type) {
            case 1:
                txtTitle.setText(context.getResources().getString(R.string.phone));
                edtInfo.setHint(context.getResources().getString(R.string.input_phone));
                imgIcon.setImageResource(R.drawable.phonecall);
                edtInfo.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case 2:
                txtTitle.setText(context.getResources().getString(R.string.input_email));
                edtInfo.setHint(context.getResources().getString(R.string.input_email));
                imgIcon.setImageResource(R.drawable.email);
                edtInfo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                break;
            default:
                break;
        }

        otp1.addTextChangedListener(new GenericTextWatcher(otp2, otp1));
        otp2.addTextChangedListener(new GenericTextWatcher(otp3, otp1));
        otp3.addTextChangedListener(new GenericTextWatcher(otp4, otp2));
        otp4.addTextChangedListener(new GenericTextWatcher(otp5, otp3));
        otp5.addTextChangedListener(new GenericTextWatcher(otp6, otp4));
        otp6.addTextChangedListener(new GenericTextWatcher(otp6, otp5));

        sendOtp.setOnClickListener(view -> {
            if(isFirst) {
                if (sendCode) {
                    setEmailPresenter.checkData(type, edtInfo.getText().toString());
                }
            } else{
                setEmailPresenter.resendOtp(otpSession);
            }
        });

        btnUpdate.setOnClickListener(view -> {
            if (update) {
                String otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() +
                        otp4.getText().toString() + otp5.getText().toString() + otp6.getText().toString();
                Log.e("otp", "code otp: " + otp);
                if (otp.length() < 6) {
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Vui lòng nhập mã OTP");
                } else {
                    setEmailPresenter.verifyOtp(otp, otpSession);
                }
            }
        });

        btnCancel.setOnClickListener(view -> {
            dialog.dismiss();
            update = false;
            sendCode = true;
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
        });

        dialog.show();
    }

    @Override
    public void inputError(boolean b, String data) {
        if (b) {
            setEmailPresenter.setEmail(edtInfo.getText().toString());
            txtErrorInput.setVisibility(View.GONE);
        } else {
            txtErrorInput.setText(data);
            txtErrorInput.setVisibility(View.VISIBLE);
        }
    }

    String otpSession = "";

    @Override
    public void setEmailSuccess(boolean success, String message, String otpSession) {
        if (success) {
            llOtp.setVisibility(View.VISIBLE);
            this.otpSession = otpSession;
            update = true;
            btnUpdate.setBackground(context.getResources().getDrawable(R.drawable.button_outline));
            sendOtp.setBackground(context.getResources().getDrawable(R.drawable.disable_send_otp));
            countDownTimer = new CountDownTimer(60000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long time = millisUntilFinished / 1000;
                    String data = String.format("<span style=\"color:black;\">%s</span><span style=\"color:red;\">%s</span><span style=\"color:black;\">%s</span>", context.getResources().getString(R.string.noti_otp) + " (", time, "s)");
                    txtTime.setText(Html.fromHtml(data));
                    sendCode = false;
                }

                @Override
                public void onFinish() {
                    sendCode = true;
                    txtTime.setText(context.getResources().getString(R.string.finish_otp));
                    sendOtp.setBackground(context.getResources().getDrawable(R.drawable.send_otp));
                }
            }.start();
        } else{
            llOtp.setVisibility(View.GONE);
        }
        Utils.showToast(context, message);
    }

    @Override
    public void resendOtp(boolean success, String message, String otpSession) {
        if (success) {
            this.otpSession = otpSession;
        }
        Utils.showToast(context, message);
    }

    @Override
    public void verifyOtp(boolean success, String message) {
        if (success) {
            context.sendBroadcast(new Intent(AppConstance.UPDATE_USER_INFO));
            dialog.dismiss();
        }
        Utils.showToast(context, message);
    }
}
