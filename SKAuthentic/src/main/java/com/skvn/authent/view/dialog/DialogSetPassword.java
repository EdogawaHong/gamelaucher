package com.skvn.authent.view.dialog;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.DetailInfoInf;
import com.skvn.authent.listener.SetPasswordInf;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.presenter.DetailInfoPresenter;
import com.skvn.authent.presenter.SetPassPresenter;

public class DialogSetPassword implements SetPasswordInf, DetailInfoInf {
    Context context;
    DialogListener listener;
    SetPassPresenter setPassPresenter;
    DetailInfoPresenter detailInfoPresenter;

    public DialogSetPassword(Context context, DialogListener listener) {
        this.context = context;
        this.listener = listener;
        setPassPresenter = new SetPassPresenter(context, listener, this);
        detailInfoPresenter=new DetailInfoPresenter(context,this,listener);
    }

    boolean showSetPass = false, showConfirmPass = false;

    TextView txtErrorPass, txtErrorConfirmPass;
    EditText edtSetPassword, edtConfirmPassword;
    Dialog dialog;

    public void showDialogSetPassword() {
        dialog = new Dialog(context, R.style.full_screen_dialog);
        dialog.getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_password);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        edtSetPassword = dialog.findViewById(R.id.edtSetPassword);
        edtConfirmPassword = dialog.findViewById(R.id.edtConfirmPassword);
        Button btnUpdate = dialog.findViewById(R.id.btnUpdate);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        ImageView imgSetPass = dialog.findViewById(R.id.imgShowSetPass);
        ImageView imgConfirmPass = dialog.findViewById(R.id.imgShowConfirmPass);

        txtErrorPass = dialog.findViewById(R.id.txtErrorPass);
        txtErrorConfirmPass = dialog.findViewById(R.id.txtErrorConfirmPass);

        imgSetPass.setOnClickListener(view1 -> {
            showSetPass = !showSetPass;
            if (showSetPass) {
                edtSetPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                imgSetPass.setImageResource(R.drawable.icon_eyes_two);
            } else {
                edtSetPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                imgSetPass.setImageResource(R.drawable.icon_eyes_one);
            }
            edtSetPassword.setSelection(edtSetPassword.length());
        });

        imgConfirmPass.setOnClickListener(view1 -> {
            showConfirmPass = !showConfirmPass;
            if (showConfirmPass) {
                edtConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                imgConfirmPass.setImageResource(R.drawable.icon_eyes_two);
            } else {
                edtConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                imgConfirmPass.setImageResource(R.drawable.icon_eyes_one);
            }
            edtConfirmPassword.setSelection(edtConfirmPassword.length());
        });

        btnUpdate.setOnClickListener(view1 -> {
            setPassPresenter.checkPassword(edtSetPassword.getText().toString().trim(), edtConfirmPassword.getText().toString().trim());
        });

        btnCancel.setOnClickListener(view1 -> {
            dialog.dismiss();
        });

        dialog.show();
    }

    @Override
    public void inputError(boolean b, String errorPass, String errorConfirm) {
        if (!b) {
            if (errorPass != null && !errorPass.equals("")) {
                txtErrorPass.setVisibility(View.VISIBLE);
                txtErrorPass.setText(errorPass);
            } else {
                txtErrorPass.setVisibility(View.GONE);
                txtErrorPass.setText("");
            }

            if (errorConfirm != null && !errorConfirm.equals("")) {
                txtErrorConfirmPass.setVisibility(View.VISIBLE);
                txtErrorConfirmPass.setText(errorConfirm);
            } else {
                txtErrorConfirmPass.setVisibility(View.GONE);
                txtErrorConfirmPass.setText("");
            }
        } else {
            txtErrorPass.setVisibility(View.GONE);
            txtErrorConfirmPass.setVisibility(View.GONE);
            setPassPresenter.setPassword(edtSetPassword.getText().toString().trim());
        }
    }

    @Override
    public void setPassSuccess(boolean b,String message) {
        Utils.showToast(context, message);
        if(b){
            dialog.dismiss();
            Log.e("DialogUpdatePassword","success");
            detailInfoPresenter.getInfo();
            context.sendBroadcast(new Intent(AppConstance.UPDATE_USER_INFO));
        }
    }

    @Override
    public void getInfoSuccess(boolean b, UserInfo userInfo) {
        //todo broadcast
    }
}

