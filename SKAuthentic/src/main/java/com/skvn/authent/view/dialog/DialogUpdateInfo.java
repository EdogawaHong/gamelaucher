package com.skvn.authent.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.GenericTextWatcher;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.DialogUpdateInfoInf;
import com.skvn.authent.presenter.DialogUpdateInfoPresenter;

public class DialogUpdateInfo implements DialogUpdateInfoInf {
    int action = 1;
    Context context;
    DialogUpdateInfoPresenter infoPresenter;
    DialogListener listener;

    public DialogUpdateInfo(Context context, DialogListener listener) {
        this.context = context;
        this.listener = listener;
        infoPresenter = new DialogUpdateInfoPresenter(context, this.listener, this);
    }

    boolean sendCode = true, update = false;
    CountDownTimer countDownTimer = null;
    LinearLayout llOtp;
    Dialog dialog;

    TextView sendOtp;
    Button btnUpdate;
    LinearLayout llNewInfo;
    TextView txtError;
    LinearLayout llOldInfo;

    boolean isOld = true;
    boolean isFirst = true;

    String tmpEmail = "";

    public void showDialogUpdatePhoneEmail(int type, String data) {
        update = false;
        sendCode = true;
        countDownTimer = null;
        dialog = new Dialog(context, R.style.full_screen_dialog);
        dialog.getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update_phone_email);
        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final EditText edtInfo = dialog.findViewById(R.id.edtInfo);
        TextView txtTitle = dialog.findViewById(R.id.txtTitle);
        ImageView imgIcon = dialog.findViewById(R.id.imgIcon);
        ImageView imgIconNew = dialog.findViewById(R.id.imgIconNew);

        sendOtp = dialog.findViewById(R.id.txtSendOtp);
        TextView sendOtpNew = dialog.findViewById(R.id.txtSendOtpNew);
        TextView txtTime = dialog.findViewById(R.id.txtTime);

        btnUpdate = dialog.findViewById(R.id.btnUpdate);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);

        EditText otp1 = dialog.findViewById(R.id.otp1);
        EditText otp2 = dialog.findViewById(R.id.otp2);
        EditText otp3 = dialog.findViewById(R.id.otp3);
        EditText otp4 = dialog.findViewById(R.id.otp4);
        EditText otp5 = dialog.findViewById(R.id.otp5);
        EditText otp6 = dialog.findViewById(R.id.otp6);

        TextView txtOld = dialog.findViewById(R.id.txtOld);
        TextView txtNew = dialog.findViewById(R.id.txtNew);
        EditText edtNewInfo = dialog.findViewById(R.id.edtNewInfo);

        TextView txtErrorInput = dialog.findViewById(R.id.txtErrorInput);
        txtError = dialog.findViewById(R.id.txtError);


        llOldInfo = dialog.findViewById(R.id.llOldInfo);
        llOtp = dialog.findViewById(R.id.llOtp);
        llNewInfo = dialog.findViewById(R.id.llNewInfo);

        btnUpdate.setBackground(context.getResources().getDrawable(R.drawable.disable_button_update));

        txtError.setVisibility(View.GONE);
        llOtp.setVisibility(View.GONE);
        llNewInfo.setVisibility(View.GONE);
        txtOld.setVisibility(View.VISIBLE);

        switch (type) {
            case 1:
                edtInfo.setText(Utils.hidePhone(data));
                txtTitle.setText(context.getResources().getString(R.string.change_phone));
                edtInfo.setHint(context.getResources().getString(R.string.input_phone));
                edtNewInfo.setHint(context.getResources().getString(R.string.new_phone));
                imgIcon.setImageResource(R.drawable.phonecall);
                imgIconNew.setImageResource(R.drawable.phonecall);
                edtNewInfo.setInputType(InputType.TYPE_CLASS_NUMBER);
                edtInfo.setInputType(InputType.TYPE_CLASS_NUMBER);
                txtOld.setText(context.getResources().getString(R.string.old_phone));
                txtNew.setText(context.getResources().getString(R.string.new_phone));
                break;
            case 2:
                tmpEmail = data;
                edtInfo.setText(Utils.hideEmail(data));
                txtTitle.setText(context.getResources().getString(R.string.change_email));
                edtInfo.setHint(context.getResources().getString(R.string.input_email));
                edtNewInfo.setHint(context.getResources().getString(R.string.new_email));
                imgIcon.setImageResource(R.drawable.email);
                imgIconNew.setImageResource(R.drawable.email);
                edtInfo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                edtNewInfo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                txtOld.setText(context.getResources().getString(R.string.old_email));
                txtNew.setText(context.getResources().getString(R.string.new_email));
                break;
            default:
                break;
        }

        otp1.addTextChangedListener(new GenericTextWatcher(otp2, otp1));
        otp2.addTextChangedListener(new GenericTextWatcher(otp3, otp1));
        otp3.addTextChangedListener(new GenericTextWatcher(otp4, otp2));
        otp4.addTextChangedListener(new GenericTextWatcher(otp5, otp3));
        otp5.addTextChangedListener(new GenericTextWatcher(otp6, otp4));
        otp6.addTextChangedListener(new GenericTextWatcher(otp6, otp5));

        sendOtp.setOnClickListener(view -> {
            if (sendCode) {
                llOtp.setVisibility(View.VISIBLE);
                if (isFirst) {
                    isFirst = false;
                    infoPresenter.sendOtpOldEmail(tmpEmail);
                } else {
                    infoPresenter.resendOtp(otpSession);
                }

                btnUpdate.setBackground(context.getResources().getDrawable(R.drawable.button_outline));
                sendOtp.setBackground(context.getResources().getDrawable(R.drawable.disable_send_otp));
                action = 1;
                update = true;
                btnUpdate.setText(context.getResources().getString(R.string.action_confirm));

                //gửi mã thành công
                countDownTimer = new CountDownTimer(60000, 1000) {

                    @Override
                    public void onTick(long millisUntilFinished) {
                        long time = millisUntilFinished / 1000;
                        String data = String.format("<span style=\"color:black;\">%s</span><span style=\"color:red;\">%s</span><span style=\"color:black;\">%s</span>", context.getResources().getString(R.string.noti_otp) + " (", time, "s)");
                        txtTime.setText(Html.fromHtml(data));
                    }

                    @Override
                    public void onFinish() {
                        txtTime.setText(context.getResources().getString(R.string.finish_otp));
                        sendOtp.setBackground(context.getResources().getDrawable(R.drawable.send_otp));
                    }
                }.start();
            }
        });
        sendOtpNew.setOnClickListener(view -> {
            if (sendCode) {
                if (edtNewInfo.getText().toString().equals("")) {
                    txtErrorInput.setVisibility(View.VISIBLE);
                    if (type == 1) {
                        txtErrorInput.setText(R.string.not_phone);
                    } else {
                        txtErrorInput.setText(R.string.not_gmail);
                    }
                } else {
                    otp1.setText("");
                    otp2.setText("");
                    otp3.setText("");
                    otp4.setText("");
                    otp5.setText("");
                    otp6.setText("");
                    txtErrorInput.setVisibility(View.GONE);
                    llOtp.setVisibility(View.VISIBLE);
                    btnUpdate.setBackground(context.getResources().getDrawable(R.drawable.button_outline));
                    sendOtp.setBackground(context.getResources().getDrawable(R.drawable.disable_send_otp));
                    action = 1;
                    update = true;
                    btnUpdate.setText(context.getResources().getString(R.string.action_confirm));
                    if (isFirst) {
                        isFirst = false;
                        infoPresenter.sendOtpNewEmail(edtNewInfo.getText().toString(), otpSession);
                    } else {
                        infoPresenter.resendOtp(otpSession);
                    }

                    //gửi mã thành công
                    countDownTimer = new CountDownTimer(60000, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            long time = millisUntilFinished / 1000;
                            String data = String.format("<span style=\"color:black;\">%s</span><span style=\"color:red;\">%s</span><span style=\"color:black;\">%s</span>", context.getResources().getString(R.string.noti_otp) + " (", time, "s)");
                            txtTime.setText(Html.fromHtml(data));
                        }

                        @Override
                        public void onFinish() {
                            txtTime.setText(context.getResources().getString(R.string.finish_otp));
                            sendOtp.setBackground(context.getResources().getDrawable(R.drawable.send_otp));
                        }
                    }.start();
                }
            }
        });

        btnUpdate.setOnClickListener(view -> {
            if (update) {
                switch (action) {
                    case 1:
                        String otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() +
                                otp4.getText().toString() + otp5.getText().toString() + otp6.getText().toString();
                        Log.e("SecurityFragment", "code otp: " + otp);
                        if (otp.length() < 6) {
                            txtError.setVisibility(View.VISIBLE);
                            txtError.setText("Vui lòng nhập mã OTP");
                        } else {
                            //kiểm tra otp -> thành công
                            infoPresenter.verifyOtp(otp, otpSession, isOld);
                        }
                        break;
                    case 2:
                        Log.e("SecurityFragment", "update");
                        break;
                    default:
                        break;
                }
            }
        });

        btnCancel.setOnClickListener(view -> {
            dialog.dismiss();
            update = false;
            sendCode = true;
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
        });

        dialog.show();
    }

    @Override
    public void inputError(boolean b, String input) {

    }

    @Override
    public void verifyEmailSuccess(boolean success, String message, String otpSession, boolean oldEmail) {
        if (success) {
            this.otpSession = otpSession;
        } else {
            llOtp.setVisibility(View.GONE);
        }
        Utils.showToast(context, message);
    }

    String otpSession = "";

    @Override
    public void resendOtp(boolean success, String message, String otpSession) {
        if (success) {
            this.otpSession = otpSession;
        }
        Utils.showToast(context, message);
    }

    @Override
    public void verifyOtp(boolean success, String message, boolean isOld) {
        if (success) {
            if (isOld) {
                this.isOld = false;
                isFirst = true;
                update = false;
                btnUpdate.setBackground(context.getResources().getDrawable(R.drawable.disable_button_update));
                btnUpdate.setText(context.getResources().getString(R.string.action_update));
                action = 2;
                llNewInfo.setVisibility(View.VISIBLE);
                txtError.setVisibility(View.GONE);
                llOldInfo.setVisibility(View.GONE);
                llOtp.setVisibility(View.GONE);
                sendCode = true;
            } else {
                context.sendBroadcast(new Intent(AppConstance.UPDATE_USER_INFO));
                dialog.dismiss();
            }
        }
        Utils.showToast(context, message);
    }
}
