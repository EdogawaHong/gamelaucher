package com.skvn.authent.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.skvn.authent.R;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.UpdatePasswordInf;
import com.skvn.authent.presenter.UpdatePassPresenter;

public class DialogUpdatePassword implements UpdatePasswordInf {
    DialogListener listener;
    Context context;

    Dialog dialog;

    UpdatePassPresenter updatePassPresenter;

    public DialogUpdatePassword(Context context, DialogListener listener) {
        this.listener = listener;
        this.context = context;
        updatePassPresenter = new UpdatePassPresenter(context, listener, this);
    }

    boolean showOldPass = false, showNewPass = false, showConfirmNewPass = false;
    TextView txtErrorOldPass, txtErrorNewPass, txtErrorConfirmPass;
    EditText edtOldPassword, edtNewPassword, edtConfirmNewPassword;

    public void showDialogChangePassword() {
        dialog = new Dialog(context, R.style.full_screen_dialog);
        dialog.getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update_password);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        edtOldPassword = dialog.findViewById(R.id.edtOldPassword);
        edtNewPassword = dialog.findViewById(R.id.edtNewPassword);
        edtConfirmNewPassword = dialog.findViewById(R.id.edtConfirmNewPassword);
        Button btnUpdate = dialog.findViewById(R.id.btnUpdate);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        ImageView imgOldPass = dialog.findViewById(R.id.imgShowOldPass);
        ImageView imgNewPass = dialog.findViewById(R.id.imgShowNewPass);
        ImageView imgConfirmNewPass = dialog.findViewById(R.id.imgShowConfirmNewPass);

        txtErrorOldPass = dialog.findViewById(R.id.txtErrorOldPass);
        txtErrorNewPass = dialog.findViewById(R.id.txtErrorNewPass);
        txtErrorConfirmPass = dialog.findViewById(R.id.txtErrorConfirmPass);

        imgOldPass.setOnClickListener(view1 -> {
            showOldPass = !showOldPass;
            if (showOldPass) {
                edtOldPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                imgOldPass.setImageResource(R.drawable.icon_eyes_two);
            } else {
                edtOldPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                imgOldPass.setImageResource(R.drawable.icon_eyes_one);
            }
            edtOldPassword.setSelection(edtOldPassword.length());
        });

        imgNewPass.setOnClickListener(view1 -> {
            showNewPass = !showNewPass;
            if (showNewPass) {
                edtNewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                imgNewPass.setImageResource(R.drawable.icon_eyes_two);
            } else {
                edtNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                imgNewPass.setImageResource(R.drawable.icon_eyes_one);
            }
            edtNewPassword.setSelection(edtNewPassword.length());
        });

        imgConfirmNewPass.setOnClickListener(view1 -> {
            showConfirmNewPass = !showConfirmNewPass;
            if (showConfirmNewPass) {
                edtConfirmNewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                imgConfirmNewPass.setImageResource(R.drawable.icon_eyes_two);
            } else {
                edtConfirmNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                imgConfirmNewPass.setImageResource(R.drawable.icon_eyes_one);
            }
            edtConfirmNewPassword.setSelection(edtConfirmNewPassword.length());
        });

        btnUpdate.setOnClickListener(view1 -> {
//            passwordPresenter.updatePassword(edtOldPassword.getText().toString(), edtNewPassword.getText().toString(), edtConfirmNewPassword.getText().toString());
            updatePassPresenter.checkPassword(edtOldPassword.getText().toString().trim(),
                    edtNewPassword.getText().toString().trim(),
                    edtConfirmNewPassword.getText().toString().trim());
        });

        btnCancel.setOnClickListener(view1 -> {
            dialog.dismiss();
        });

        dialog.show();
    }

    @Override
    public void inputError(boolean b, String errorOld, String errorNew, String errorConfirm) {
        if (!b) {
            Log.e("checkpass","check pass false");
            if (errorOld != null && !errorOld.equals("")) {
                txtErrorOldPass.setVisibility(View.VISIBLE);
                txtErrorOldPass.setText(errorOld);
            } else {
                txtErrorOldPass.setVisibility(View.GONE);
                txtErrorOldPass.setText("");
            }
            if (errorNew != null && !errorNew.equals("")) {
                txtErrorNewPass.setVisibility(View.VISIBLE);
                txtErrorNewPass.setText(errorNew);
            } else {
                txtErrorNewPass.setVisibility(View.GONE);
                txtErrorNewPass.setText("");
            }
            if (errorConfirm != null && !errorConfirm.equals("")) {
                txtErrorConfirmPass.setVisibility(View.VISIBLE);
                txtErrorConfirmPass.setText(errorConfirm);
            } else {
                txtErrorConfirmPass.setVisibility(View.GONE);
                txtErrorConfirmPass.setText("");
            }
        } else {
            txtErrorOldPass.setVisibility(View.GONE);
            txtErrorNewPass.setVisibility(View.GONE);
            txtErrorConfirmPass.setVisibility(View.GONE);
            updatePassPresenter.updatePassword(edtOldPassword.getText().toString(), edtNewPassword.getText().toString(), edtConfirmNewPassword.getText().toString());
        }
    }

    @Override
    public void changePassSuccess(boolean success, String message) {
        Utils.showToast(context, message);
        if(success){
            dialog.dismiss();
            Log.e("DialogUpdatePassword","success");
        }
    }
}
