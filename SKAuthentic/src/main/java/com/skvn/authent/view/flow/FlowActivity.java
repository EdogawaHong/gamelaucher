package com.skvn.authent.view.flow;

//import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.view.headService.headinfo.HeadInfoActivity;
import com.skvn.authent.view.home.HomeActivity;
//
//public class FlowActivity extends AppCompatActivity {
//
//    Context context;
//    boolean isVerified = false;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        context = this;
//        check();
//    }
//
//    void check(){
//        String session = SharedUtils.getInstance(context).getStringValue(AppConstance.TOKEN);
//        if (session == null || session.length() == 0) {
//            isVerified = false;
//        } else {
//            isVerified = true;
//        }
//        Intent intent = null;
//        if (isVerified) {
//            //Open the chat conversation click.
//             intent = new Intent(this, HeadInfoActivity.class);
//        } else {
//             intent = new Intent(context, HomeActivity.class);
//        }
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        finish();
//    }
//}