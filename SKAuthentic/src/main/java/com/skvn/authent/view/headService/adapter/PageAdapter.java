package com.skvn.authent.view.headService.adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.JsonHelper;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.view.headService.fragment.SecurityFragment;
import com.skvn.authent.view.headService.fragment.InfoFragment;

public class PageAdapter extends FragmentPagerAdapter {
    Context context;
    UserInfo userInfo;

    public PageAdapter(@NonNull FragmentManager fm, Context nContext) {
        super(fm);
        context = nContext;
        String json_user = SharedUtils.getInstance(context).getStringValue(AppConstance.USER_INFO);
        if (json_user != null && json_user.length() > 0) {
            UserInfo userInfo = JsonHelper.getObject(json_user, UserInfo.class);
            this.userInfo = userInfo;
        }else{
            this.userInfo = null;
        }
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putString("user_info", JsonHelper.toJson(userInfo));
        switch (position) {
            case 0:
                InfoFragment infoFragment = new InfoFragment();
                infoFragment.setArguments(bundle);
                return infoFragment;
            case 1:
                SecurityFragment securityFragment = new SecurityFragment();
                securityFragment.setArguments(bundle);
                return securityFragment;
            default:
                return null;
        }
        //return (position == 0) ? InfoFragment.newInstance(): SecurityFragment.newInstance();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String security = context.getResources().getString(R.string.security);
        String info = context.getResources().getString(R.string.info);
        return (position == 0) ? info : security;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
