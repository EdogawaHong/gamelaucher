package com.skvn.authent.view.headService.changeInfo;

import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.skvn.authent.R;

public class ChangeInfoActivity extends AppCompatActivity {
    Button confirm, exit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_user_info);
        initView();
    }

    void initView(){
        exit = findViewById(R.id.exit);
        confirm = findViewById(R.id.confirm_button);
        exit.setOnClickListener(view -> finish());

    }
}
