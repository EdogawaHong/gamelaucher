package com.skvn.authent.view.headService.changePhone;

import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.skvn.authent.R;

public class ChangePhoneActivity extends AppCompatActivity {
    Button confirm, exit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_change_phone);
        initView();
    }

    void initView(){
        exit = findViewById(R.id.exit);
        confirm = findViewById(R.id.confirm_button);
        exit.setOnClickListener(view -> finish());

    }
}
