package com.skvn.authent.view.headService.change_pass;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.GenericTextWatcher;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.ChangePasswordInf;
import com.skvn.authent.model.res.ResReqForgot;
import com.skvn.authent.presenter.ChangePassPresent;
import com.skvn.authent.view.base.BaseActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangePassActivity extends BaseActivity implements ChangePasswordInf {
    Button confirm, exit;

    LinearLayout llOtp;
    LinearLayout ln_verify_mail;
    LinearLayout ln_pass;

    TextView sendOtp;

    EditText edtInfo;

    ImageView imgShowPass;
    ImageView imgShowPassRe;

    EditText edtPassword;
    EditText edtPasswordRe;

    TextView txtTime;

    ChangePassPresent presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_change_pass);
        presenter = new ChangePassPresent(this, this);
        initView();
    }

    void initView() {
        exit = findViewById(R.id.exit);
        confirm = findViewById(R.id.confirm_button);

        exit.setOnClickListener(view -> {
            cancelTimer();
            finish();
        });

        edtInfo = findViewById(R.id.edtInfo);
        sendOtp = findViewById(R.id.txtSendOtp);

        edtPassword = findViewById(R.id.edtPassword);
        edtPasswordRe = findViewById(R.id.edtPasswordRe);

        llOtp = findViewById(R.id.llOtp);
        ln_verify_mail = findViewById(R.id.ln_verify_mail);
        ln_pass = findViewById(R.id.ln_pass);

        txtTime = findViewById(R.id.txtTime);

        EditText otp1 = findViewById(R.id.otp1);
        EditText otp2 = findViewById(R.id.otp2);
        EditText otp3 = findViewById(R.id.otp3);
        EditText otp4 = findViewById(R.id.otp4);
        EditText otp5 = findViewById(R.id.otp5);
        EditText otp6 = findViewById(R.id.otp6);

        otp1.addTextChangedListener(new GenericTextWatcher(otp2, otp1));
        otp2.addTextChangedListener(new GenericTextWatcher(otp3, otp1));
        otp3.addTextChangedListener(new GenericTextWatcher(otp4, otp2));
        otp4.addTextChangedListener(new GenericTextWatcher(otp5, otp3));
        otp5.addTextChangedListener(new GenericTextWatcher(otp6, otp4));
        otp6.addTextChangedListener(new GenericTextWatcher(otp6, otp5));
        confirm.setEnabled(false);
        sendOtp.setOnClickListener(view -> {
            if (sendCode) {
                if (isFirst) {
                    showLoading(true);
                    presenter.sendEmailOtp(edtInfo.getText().toString());
                } else {
                    showLoading(true);
                    presenter.resendOtp(otpSession);
                }
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSetPass){
                    if (checkPass()){
                        presenter.resetPassword(edtPassword.getText().toString(), otpSession);
                    }
                }
                if (update) {
                    String otp = otp1.getText().toString() + otp2.getText().toString() + otp3.getText().toString() +
                            otp4.getText().toString() + otp5.getText().toString() + otp6.getText().toString();
                    Log.e("otp", "code otp: " + otp);
                    if (otp.length() < 6) {
                        Utils.showToast(ChangePassActivity.this, "Vui lòng nhập mã OTP");
                    } else {
                        presenter.verifyOtp(otp, otpSession, null);
                    }
                }

            }
        });

    }

    private boolean checkPass() {
        if (edtPassword.getText().toString() == null || edtPassword.getText().toString().length() == 0 || edtPassword.getText().toString().length() < 6){
            Utils.showToast(this, "Mật khẩu mới không hợp lệ. Vui lòng thử lại sau!");
            return false;
        }

        if (edtPasswordRe.getText().toString() == null || edtPasswordRe.getText().toString().length() == 0 || edtPasswordRe.getText().toString().length() < 6){
            Utils.showToast(this, "Nhập lại mật khẩu mới không hợp lệ. Vui lòng thử lại sau!");
            return false;
        }

        if (!edtPassword.getText().toString().equals(edtPasswordRe.getText().toString())){
            Utils.showToast(this, "Mật khẩu không trùng khớp. Vui lòng thử lại sau!");
            return false;
        }
        return true;
    }

    boolean checkEmail() {
        Pattern pattern = Pattern.compile("^.+@.+\\..+$");
        Matcher matcher = pattern.matcher(edtInfo.getText().toString());
        if (matcher.matches()) {
            return true;
        } else {
            Utils.showToast(this, "Email không hợp lệ");
            return false;
        }
    }

    CountDownTimer countDownTimer = null;
    boolean sendCode = true;
    boolean update = false;
    boolean isFirst = true;
    String otpSession = "";
    String cusId = "";

//    void startTimer() {
//        countDownTimer = new CountDownTimer(60000, 1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                long time = millisUntilFinished / 1000;
//                String data = String.format("<span style=\"color:black;\">%s</span><span style=\"color:red;\">%s</span><span style=\"color:black;\">%s</span>", getResources().getString(R.string.noti_otp) + " (", time, "s)");
//                txtTime.setText(Html.fromHtml(data));
//                sendCode = false;
//            }
//
//            @Override
//            public void onFinish() {
//                sendCode = true;
//                txtTime.setText(getResources().getString(R.string.finish_otp));
//                sendOtp.setBackground(getResources().getDrawable(R.drawable.send_otp));
//            }
//        }.start();
//    }

    void cancelTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @Override
    public void inputError(boolean b, String errorPass, String errorConfirm) {

    }

    @Override
    public void setPassSuccess(boolean b, String message) {

    }

    @Override
    public void onPreload() {
        showLoading(true);
    }

    @Override
    public void onSuccess() {
        showLoading(false);
    }

    @Override
    public void verifyEmailSuccess(boolean success, String message, ResReqForgot data) {
        Utils.showToast(this, message);
        showLoading(false);
        if (success) {
            otpSession = data.getSession();
            cusId = data.getCustId();
            llOtp.setVisibility(View.VISIBLE);
            update = true;
            confirm.setEnabled(true);
            sendOtp.setBackground(getResources().getDrawable(R.drawable.disable_send_otp));
            countDownTimer = new CountDownTimer(60000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long time = millisUntilFinished / 1000;
                    String data = String.format("<span style=\"color:black;\">%s</span><span style=\"color:red;\">%s</span><span style=\"color:black;\">%s</span>", getResources().getString(R.string.noti_otp) + " (", time, "s)");
                    txtTime.setText(Html.fromHtml(data));
                    sendCode = false;
                }

                @Override
                public void onFinish() {
                    sendCode = true;
                    txtTime.setText(getResources().getString(R.string.finish_otp));
                    sendOtp.setBackground(getResources().getDrawable(R.drawable.send_otp));
                }
            }.start();
        }
    }

    @Override
    public void resendOtp(boolean success, String message, String data) {
        showLoading(false);
        Utils.showToast(this, message);
        if (success) {
            otpSession = data;
            sendOtp.setBackground(getResources().getDrawable(R.drawable.disable_send_otp));
            countDownTimer = new CountDownTimer(60000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long time = millisUntilFinished / 1000;
                    String data = String.format("<span style=\"color:black;\">%s</span><span style=\"color:red;\">%s</span><span style=\"color:black;\">%s</span>", getResources().getString(R.string.noti_otp) + " (", time, "s)");
                    txtTime.setText(Html.fromHtml(data));
                    sendCode = false;
                }

                @Override
                public void onFinish() {
                    sendCode = true;
                    txtTime.setText(getResources().getString(R.string.finish_otp));
                    sendOtp.setBackground(getResources().getDrawable(R.drawable.send_otp));
                }
            }.start();
        }
    }

    boolean isSetPass = false;
    @Override
    public void verifyOtp(boolean success, String message) {
        if (success) {
            llOtp.setVisibility(View.GONE);
            ln_verify_mail.setVisibility(View.GONE);
            ln_pass.setVisibility(View.VISIBLE);
            isSetPass = true;
            update = false;
        }
        Utils.showToast(this, message);
    }

    @Override
    public void resetPassword(boolean success, String message) {
        if (success)
            finish();

        Utils.showToast(this, message);
    }
}
