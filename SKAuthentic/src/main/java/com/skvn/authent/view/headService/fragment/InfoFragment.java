package com.skvn.authent.view.headService.fragment;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.JsonHelper;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.DetailInfoInf;
import com.skvn.authent.listener.UpdateInfoInf;
import com.skvn.authent.model.req.ReqUserInfo;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.presenter.DetailInfoPresenter;
import com.skvn.authent.presenter.UpdateInfoPresenter;
import com.skvn.authent.view.base.BaseFragment;
import com.skvn.authent.view.dialog.DialogListener;
import com.skvn.authent.view.dialog.DialogLoading;
import com.skvn.authent.view.headService.changeEmail.ChangeEmailActivity;
import com.skvn.authent.view.headService.changeInfo.ChangeInfoActivity;
import com.skvn.authent.view.headService.changePhone.ChangePhoneActivity;
import com.skvn.authent.view.headService.change_pass.ChangePassActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class InfoFragment extends BaseFragment implements UpdateInfoInf, DetailInfoInf {

    UserInfo userInfo;
    UpdateInfoPresenter _presenter;
    Date dateTime;
    DetailInfoPresenter _infoPresenter;

    public InfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CoinFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
            String param = SharedUtils.getInstance(getContext()).getStringValue(AppConstance.USER_INFO);
//            String param=getArguments().getString("user_info");
            Log.e("InfoFragment",param);
            if(param!=null){
                userInfo= JsonHelper.getObject(param, UserInfo.class);
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false);
    }

    EditText edtName, edtBirthday, edtCmnd;
    ImageView imgDatePicker;
    RadioButton rbMale, rbFemale, rbOther;
    Button btnUpdate, btnExit;
    TextView txtErrorName, txtErrorBirthday, txtErrorCmnd;

    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat sdf = new SimpleDateFormat(AppConstance.formatDate);
            String birthday = sdf.format(myCalendar.getTime());
            dateTime=myCalendar.getTime();
            edtBirthday.setText(birthday);
        }

    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //requireActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        getActivity().registerReceiver(updateUserInfo, new IntentFilter(AppConstance.UPDATE_USER_INFO));

        edtName = view.findViewById(R.id.edtName);
        edtBirthday = view.findViewById(R.id.edtBirthday);
        edtCmnd = view.findViewById(R.id.edtCMND);
        imgDatePicker = view.findViewById(R.id.datepicker);
        rbMale = view.findViewById(R.id.radioMale);
        rbFemale = view.findViewById(R.id.radioFemale);
        rbOther = view.findViewById(R.id.radioOther);
        btnUpdate = view.findViewById(R.id.btnUpdate);
        btnExit = view.findViewById(R.id.btnExit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        txtErrorName=view.findViewById(R.id.txtErrorName);
        txtErrorBirthday=view.findViewById(R.id.txtErrorBirthday);
        txtErrorCmnd=view.findViewById(R.id.txtErrorCmnd);

        edtBirthday.setOnClickListener(view1 -> {
            new DatePickerDialog(getContext(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        });

        rbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rbMale.isChecked();
            }
        });
        rbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rbFemale.isChecked();
            }
        });
        rbMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rbOther.isChecked();
            }
        });

        Utils.getDeviceInfo();
        String uuid= Utils.getUUID(getContext());

        setDataUser();

        _presenter=new UpdateInfoPresenter(getContext(), this, new DialogListener() {
            @Override
            public void onPreload() {
                showLoading(true);
            }

            @Override
            public void onSuccess() {
                showLoading(false);
            }

            @Override
            public void onCancel() {
                showLoading(false);
            }
        });

        _infoPresenter=new DetailInfoPresenter(getContext(), this, new DialogListener() {
            @Override
            public void onPreload() {
                showLoading(true);
            }

            @Override
            public void onSuccess() {
                showLoading(false);
            }

            @Override
            public void onCancel() {
                showLoading(false);
            }
        });

        btnUpdate.setOnClickListener(view1 -> {
//                    if (rbMale.isChecked())
//                        Toast.makeText(getContext(), "giới tính nam", Toast.LENGTH_SHORT).show();
//                    if (rbFemale.isChecked())
//                        Toast.makeText(getContext(), "giới tính nữ", Toast.LENGTH_SHORT).show();
//                    if (rbOther.isChecked())
//                        Toast.makeText(getContext(), "giới tính khác", Toast.LENGTH_SHORT).show();
            _presenter.checkInfo(edtName.getText().toString(),edtBirthday.getText().toString(),edtCmnd.getText().toString());
                }
        );
    }

    void setDataUser(){
        if(userInfo!=null){
            if(userInfo.getFullName()!=null && !userInfo.getFullName().isEmpty()) {
                edtName.setText(userInfo.getFullName());
            }
            if(userInfo.getBirthday()!=null && !userInfo.getBirthday().isEmpty()) {
                DateFormat df = new SimpleDateFormat(AppConstance.formatDateTime);
                try {
                    dateTime = df.parse(userInfo.getBirthday());
                    String birthday = new SimpleDateFormat(AppConstance.formatDate).format(dateTime);
                    edtBirthday.setText(birthday);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if(userInfo.getCmndNumber()!=null && !userInfo.getCmndNumber().isEmpty()) {
                edtCmnd.setText(userInfo.getCmndNumber());
            }
        }
    }

    @Override
    public void inputError(boolean b, String errorName, String errorBirthday, String errorCmnd) {
        if (!b) {
            if (errorName != null && !errorName.equals("")) {
                txtErrorName.setVisibility(View.VISIBLE);
                txtErrorName.setText(errorName);
            } else {
                txtErrorName.setVisibility(View.GONE);
                txtErrorName.setText("");
            }
            if (errorBirthday != null && !errorBirthday.equals("")) {
                txtErrorBirthday.setVisibility(View.VISIBLE);
                txtErrorBirthday.setText(errorBirthday);
            } else {
                txtErrorBirthday.setVisibility(View.GONE);
                txtErrorBirthday.setText("");
            }
            if (errorCmnd != null && !errorCmnd.equals("")) {
                txtErrorCmnd.setVisibility(View.VISIBLE);
                txtErrorCmnd.setText(errorCmnd);
            } else {
                txtErrorCmnd.setVisibility(View.GONE);
                txtErrorCmnd.setText("");
            }
        } else {
            txtErrorName.setVisibility(View.GONE);
            txtErrorBirthday.setVisibility(View.GONE);
            txtErrorCmnd.setVisibility(View.GONE);

            ReqUserInfo req=new ReqUserInfo();
            req.setFullName(edtName.getText().toString());
            if(dateTime!=null) {
                String birthday = new SimpleDateFormat(AppConstance.formatDateTime).format(dateTime);
                req.setBirthday(birthday);
            }
            req.setCmndNumber(edtCmnd.getText().toString());
            _presenter.updateInfo(req);
        }
    }

    @Override
    public void updateSuccess(boolean success, String message) {
        if(success){
            _infoPresenter.getInfo();
            Utils.showToast(getContext(), "Cập nhật thông tin thành công!");
        } else{
            Utils.showToast(getContext(), message);
        }
    }

    BroadcastReceiver updateUserInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AppConstance.UPDATE_USER_INFO)){
                _infoPresenter.getInfo();
            }
        }
    };

    @Override
    public void getInfoSuccess(boolean b,UserInfo userInfo) {
        if(b){
            this.userInfo=userInfo;
            getActivity().sendBroadcast(new Intent(AppConstance.LOAD_USER_INFO).putExtra(AppConstance.LOAD_USER_INFO, this.userInfo));
            setDataUser();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(updateUserInfo);
    }
}