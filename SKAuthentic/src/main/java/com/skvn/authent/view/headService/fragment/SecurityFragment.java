package com.skvn.authent.view.headService.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.JsonHelper;
import com.skvn.authent.common.Utils;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.view.base.BaseFragment;
import com.skvn.authent.view.dialog.DialogListener;
import com.skvn.authent.view.dialog.DialogSetInfo;
import com.skvn.authent.view.dialog.DialogSetPassword;
import com.skvn.authent.view.dialog.DialogUpdateInfo;
import com.skvn.authent.view.dialog.DialogUpdatePassword;

public class SecurityFragment extends BaseFragment {

    UserInfo userInfo;

    public SecurityFragment() {
    }

    public static SecurityFragment newInstance() {
        SecurityFragment fragment = new SecurityFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
            if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
                String param = getArguments().getString("user_info");
                Log.e("InfoFragment", param);
                if (param != null) {
                    UserInfo user = JsonHelper.getObject(param, UserInfo.class);
                    userInfo = user;
                }
            }
        }
    }


    BroadcastReceiver reloadInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AppConstance.LOAD_USER_INFO)){
                UserInfo info = (UserInfo) intent.getSerializableExtra(AppConstance.LOAD_USER_INFO);
                if (info != null) {
                    userInfo = info;
                    setData();
                }
            }
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_security, container, false);
    }

    Button btnPassword, btnPhone, btnEmail;
    EditText edtPassword, edtPhone, edtEmail;
    ImageView imgStatus;
    TextView txtStatus;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getContext().registerReceiver(reloadInfo, new IntentFilter(AppConstance.LOAD_USER_INFO));
        //requireActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

        btnPassword = view.findViewById(R.id.change_pass);
        btnPhone = view.findViewById(R.id.change_phone);
        btnEmail = view.findViewById(R.id.change_email);
        edtPassword = view.findViewById(R.id.edtPassword);
        edtPhone = view.findViewById(R.id.edtPhone);
        edtEmail = view.findViewById(R.id.edtEmail);
        imgStatus = view.findViewById(R.id.imgStatus);
        txtStatus = view.findViewById(R.id.txtStatus);

        setData();

        setStatus();
    }

    void setData() {
        if (userInfo != null) {
            DialogSetInfo dialogSetInfo = new DialogSetInfo(getContext(), new DialogListener() {
                @Override
                public void onPreload() {
                    showLoading(true);
                }

                @Override
                public void onSuccess() {
                    showLoading(false);
                }

                @Override
                public void onCancel() {
                    showLoading(false);
                }
            });
            DialogUpdateInfo dialogUpdateInfo = new DialogUpdateInfo(getContext(), new DialogListener() {
                @Override
                public void onPreload() {
                    showLoading(true);
                }

                @Override
                public void onSuccess() {
                    showLoading(false);
                }

                @Override
                public void onCancel() {
                    showLoading(false);
                }
            });

            if (userInfo.getPasswordStatus() == 1) {
                edtPassword.setText("********");
                btnPassword.setText(getResources().getString(R.string.change_password));
//                if (edtPassword.getText().toString().equals("") || edtPassword.getText() == null) {
//
//                } else {
//
//                }
            } else{
                btnPassword.setText(getResources().getString(R.string.set_password));
            }

            btnPassword.setOnClickListener(view1 -> {
                if (userInfo.getPasswordStatus() == 0) {
                    new DialogSetPassword(getContext(), new DialogListener() {
                        @Override
                        public void onPreload() {
                            showLoading(true);
                        }

                        @Override
                        public void onSuccess() {
                            showLoading(false);
                        }

                        @Override
                        public void onCancel() {
                            showLoading(false);
                        }
                    }).showDialogSetPassword();
                } else {
                    new DialogUpdatePassword(getContext(), new DialogListener() {
                        @Override
                        public void onPreload() {
                            showLoading(true);
                        }

                        @Override
                        public void onSuccess() {
                            showLoading(false);
                        }

                        @Override
                        public void onCancel() {
                            showLoading(false);
                        }
                    }).showDialogChangePassword();
                }
            });

            if (userInfo.getPhone() != null && !userInfo.getPhone().equals("")) {
                String phone = userInfo.getPhone();
                edtPhone.setText(Utils.hidePhone(phone));
                btnPhone.setText(getResources().getString(R.string.action_change));
                btnPhone.setOnClickListener(view1 -> {
                    dialogUpdateInfo.showDialogUpdatePhoneEmail(1, phone);
                });
            } else {
                btnPhone.setText(getResources().getString(R.string.action_update));
                btnPhone.setOnClickListener(view1 -> {
                    dialogSetInfo.showDialogPhoneEmail(1);
                });
            }

            if(userInfo.getEmail()!=null && !userInfo.getEmail().equals("")){
                String email=userInfo.getEmail();
                edtEmail.setText(Utils.hideEmail(email));
                btnEmail.setText(getResources().getString(R.string.action_change));
                btnEmail.setOnClickListener(view1 -> {
                    dialogUpdateInfo.showDialogUpdatePhoneEmail(2, email);
                });
            }else{
                btnEmail.setText(getResources().getString(R.string.action_update));
                btnEmail.setOnClickListener(view1 -> {
                        dialogSetInfo.showDialogPhoneEmail(2);
                });
            }

//            String email = "Nguyenhong0212@gmail.com";
//            edtEmail.setText(Utils.hideEmail(email));
//            if (edtEmail.getText().toString().equals("")) {
//                btnEmail.setText(getResources().getString(R.string.action_update));
//            } else {
//                btnEmail.setText(getResources().getString(R.string.action_change));
//            }
//            btnEmail.setOnClickListener(view1 -> {
//                if (edtEmail.getText().toString().equals(""))
//                    dialogSetInfo.showDialogPhoneEmail(2);
//                else dialogUpdateInfo.showDialogUpdatePhoneEmail(2, email);
//            });
        }

    }

    void setStatus() {
        if (edtPassword.getText().length() > 0  && edtEmail.getText().length() > 0 &&
                edtPassword.getText() != null  && edtEmail.getText() != null) {
            imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_check_circle_24));
            txtStatus.setText(getResources().getString(R.string.des_info));
        } else {
            imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_info_24));
            txtStatus.setText(getResources().getString(R.string.des_update_info));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(reloadInfo);
    }
}