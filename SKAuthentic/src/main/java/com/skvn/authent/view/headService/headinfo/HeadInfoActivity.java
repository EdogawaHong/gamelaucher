package com.skvn.authent.view.headService.headinfo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
//import com.google.android.gms.auth.api.signin.GoogleSignIn;
//import com.google.android.gms.auth.api.signin.GoogleSignInClient;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
//import com.google.firebase.auth.FirebaseAuth;
import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.model.base.CallbackResponse;
import com.skvn.authent.model.res.UserInfo;
import com.skvn.authent.view.SkyGameSdkAuthentActivity;
import com.skvn.authent.view.base.BaseActivity;
import com.skvn.authent.view.base.CustomPager;
import com.skvn.authent.view.headService.adapter.PageAdapter;

public class HeadInfoActivity extends BaseActivity implements HeadInfoInf {

    ImageView close, logout;
    TabLayout tab_layout;
    CustomPager vp2;
    PageAdapter pageAdapter;
    GoogleSignInClient mGoogleSignInClient;
    HeadInfoPresenter presenter;
    Context context;

    TextView tv_name;
    TextView tv_id;
    TextView tv_coin;
    ImageView avatar;

    void setUserInfo(UserInfo userInfo) {
        tv_name.setText((userInfo.getFullName() != null && userInfo.getFullName().length() > 0) ? userInfo.getFullName() : "Chưa xác định");
        tv_id.setText((userInfo.getAccount() != null && userInfo.getAccount().length() > 0) ? userInfo.getAccount() : "Chưa xác định");
//        tv_coin.setText((userInfo.getAccount() != null && userInfo.getAccount().length() > 0) ? userInfo.getAccount() : "Chưa xác định");
        if (userInfo.getAvatar() != null && userInfo.getAvatar().length() > 0) {
            Glide.with(this).load(userInfo.getAvatar()).into(avatar);
        }
    }

    BroadcastReceiver logoutInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AppConstance.LOGOUT)) {
                logoutSuccess(true);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.layout_info);
        initializingGoogleSDK();
        initView();

        presenter = new HeadInfoPresenter(this, this);
        presenter.loadUserData(context);
        registerReceiver(logoutInfo, new IntentFilter(AppConstance.LOGOUT));
        presenter.getInfo();
        Log.e(TAG, "SKYWAY SDK - USER INFO SCENE");
    }

    private static final String TAG = "HeadInfoActivity";

    void initView() {
        close = findViewById(R.id.close);
        logout = findViewById(R.id.logout);
        tab_layout = findViewById(R.id.tab_layout);
        vp2 = findViewById(R.id.vp2);
        close.setOnClickListener(view -> onBackPressed());
        logout.setOnClickListener(view -> {
            presenter.logout("1");
        });

        tv_name = findViewById(R.id.tv_name);
        tv_id = findViewById(R.id.tv_id);
        tv_coin = findViewById(R.id.tv_coin);
        avatar = findViewById(R.id.avatar);

        pageAdapter = new PageAdapter(getSupportFragmentManager(), getBaseContext());
        vp2.setAdapter(pageAdapter);
        tab_layout.setupWithViewPager(vp2);
    }

    void initializingGoogleSDK() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    public void getInfoSuccess(boolean success, UserInfo userInfo) {
        if (success) {
            setUserInfo(userInfo);
        }
    }

    @Override
    public void logoutSuccess(boolean success) {
//        String main_launcher = SharedUtils.getInstance(this).getStringValue(AppConstance.MAIN_ACTIVITY_LAUNCHER);
        SharedUtils.getInstance(getBaseContext()).clearData();
//        SharedUtils.getInstance(this).putStringValue(AppConstance.MAIN_ACTIVITY_LAUNCHER, main_launcher);

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();
//                FirebaseAuth.getInstance().signOut();
            }
        }).executeAsync();
        //LoginManager.getInstance().logOut();
        //FirebaseAuth.getInstance().signOut();
        mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
//                    FirebaseAuth.getInstance().signOut();
                }
            }
        });
        CallbackResponse response = new CallbackResponse(AppConstance.ACTION_LOGOUT, null);
        Intent intent = new Intent();
        intent.putExtra(AppConstance.CALLBACK_RESPONSE, response);
        setResult(Activity.RESULT_OK, intent);
        finish();

        Log.e(TAG, "SKYWAY SDK - LOGOUT");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(logoutInfo);
    }

    @Override
    public void onBackPressed() {
        CallbackResponse response = new CallbackResponse(AppConstance.ACTION_BACK, null);
        Intent intent = new Intent();
        intent.putExtra(AppConstance.CALLBACK_RESPONSE, response);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
