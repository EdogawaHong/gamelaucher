package com.skvn.authent.view.headService.headinfo;

import com.skvn.authent.model.res.UserInfo;

public interface HeadInfoInf {
    void logoutSuccess(boolean success);

    void getInfoSuccess(boolean success, UserInfo userInfo);
}
