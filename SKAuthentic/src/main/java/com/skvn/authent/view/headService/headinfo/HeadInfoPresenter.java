package com.skvn.authent.view.headService.headinfo;

import android.content.Context;

import com.google.gson.Gson;
import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.JsonHelper;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.req.Logout;
import com.skvn.authent.model.res.LoginData;
import com.skvn.authent.model.res.SessionCheckingResult;
import com.skvn.authent.model.res.UserInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HeadInfoPresenter {

    HeadInfoInf inf;
    APIService apiService;
    Context context;

    public HeadInfoPresenter(HeadInfoInf inf, Context context) {
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
        this.context = context;
    }

    public void getInfo() {
        String authorization = ApiUtils.getHeaderAuthorization(context);
        apiService.getUserInfo(authorization).enqueue(new Callback<BaseResponse<UserInfo>>() {
            @Override
            public void onResponse(Call<BaseResponse<UserInfo>> call, Response<BaseResponse<UserInfo>> response) {
                if (ApiUtils.notError(response.body().getError(), context)) {
                    SharedUtils.getInstance(context).putStringValue(AppConstance.USER_INFO, new Gson().toJson(response.body().getData()));
                    inf.getInfoSuccess(true, response.body().getData());
                } else {
                    inf.getInfoSuccess(false, null);
//                    Utils.showToast(context, response.body().getError().getMessage());
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<UserInfo>> call, Throwable t) {
                Utils.showToast(context, t.getMessage());
                inf.getInfoSuccess(false, null);
            }
        });
    }

    public void logout(String gameId){
        String session = SharedUtils.getInstance(context).getStringValue(AppConstance.TOKEN);
        String user_data = SharedUtils.getInstance(context).getStringValue(AppConstance.USER_DATA);
        LoginData data = JsonHelper.getObjectNoException(user_data, LoginData.class);
        String cguid = null;
        if (data != null){
            cguid = data.getCgUuid();
        }
        if (session != null || session.length() != 0){
            //Already has stored session
            Logout checking = new Logout(gameId, session, cguid);
            String authorization = ApiUtils.getHeaderAuthorization(context);
            apiService.logout(checking, authorization).enqueue(new Callback<BaseResponse<SessionCheckingResult>>() {
                @Override
                public void onResponse(Call<BaseResponse<SessionCheckingResult>> call, Response<BaseResponse<SessionCheckingResult>> response) {
                    if (ApiUtils.notError(response.body().getError(), context)) {
                        Utils.showToast(context, response.body().getError().getMessage());
                        inf.logoutSuccess(true);
                    } else{
                        inf.logoutSuccess(false);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SessionCheckingResult>> call, Throwable t) {
                    Utils.showToast(context, t.getMessage());
                    inf.logoutSuccess(false);
                }
            });
        } else{
            //Session not stored
            inf.logoutSuccess(false);
        }
    }

    public void loadUserData(Context context) {
        String json_user = SharedUtils.getInstance(context).getStringValue(AppConstance.USER_INFO);
        if (json_user != null && json_user.length() > 0){
            UserInfo userInfo = JsonHelper.getObject(json_user, UserInfo.class);
            inf.getInfoSuccess(true, userInfo);
        } else {
            inf.getInfoSuccess(false, null);
        }
    }
}
