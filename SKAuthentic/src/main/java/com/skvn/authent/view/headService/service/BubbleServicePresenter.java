package com.skvn.authent.view.headService.service;

import android.content.Context;

import com.skvn.authent.common.APIService;
import com.skvn.authent.common.ApiUtils;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.model.base.BaseResponse;
import com.skvn.authent.model.res.SessionCheckingResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BubbleServicePresenter {
    BubbleViewInf inf;
    APIService apiService;

    public BubbleServicePresenter(BubbleViewInf inf, Context context, String gameId) {
        this.inf = inf;
        apiService = ApiUtils.getAPIService();
        checkSession(context, gameId);
    }

    public void checkSession(Context context, String gameId){
        String session = SharedUtils.getInstance(context).getStringValue(AppConstance.TOKEN);
        if (session != null && session.length() != 0){
            //Already has stored session
//            SessionChecking checking = new SessionChecking(gameId, session);
            String authorization = ApiUtils.getHeaderAuthorization(context);
            apiService.checkSession(authorization, ApiUtils.BASE_VER_CODE_ANDROID, "android").enqueue(new Callback<BaseResponse<SessionCheckingResult>>() {
                @Override
                public void onResponse(Call<BaseResponse<SessionCheckingResult>> call, Response<BaseResponse<SessionCheckingResult>> response) {
                    if (ApiUtils.notError(response.body().getError(), context)) {
                       if (response.body().getData().getStatusOnline() == 1)
                        inf.resultSession(true);
                    } else{
                        inf.resultSession(false);
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse<SessionCheckingResult>> call, Throwable t) {
                    Utils.showToast(context, t.getMessage());
                    inf.resultSession(false);
                }
            });
        } else{
            //Session not stored
            inf.resultSession(false);
        }
    }
}
