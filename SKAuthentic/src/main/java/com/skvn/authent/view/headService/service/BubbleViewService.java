package com.skvn.authent.view.headService.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.core.app.NotificationCompat;

import com.skvn.authent.R;
import com.skvn.authent.view.SkyGameSdkAuthentActivity;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

//public class BubbleViewService extends Service implements BubbleViewInf {
//
//    private WindowManager mWindowManager;
//    private View mChatHeadView;
//    Integer initialX = 0, initialY = 0;
//    Float initialTouchX = 0f, initialTouchY = 0f;
//
//    int mWidth;
//
//    WindowManager.LayoutParams params;
//    LayoutInflater inflater;
//    Context context;
//    private static final String TAG = "BubbleViewService";
//
//    BubbleServicePresenter presenter;
//
//    public BubbleViewService() {
//    }
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
////    boolean isStart;
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
////        isStart = intent.getBooleanExtra("isStart", false);
//        presenter = new BubbleServicePresenter(this, context, "1");
//        startServiceOreoCondition();
//        return super.onStartCommand(intent, flags, startId);
//    }
//
//    private void startServiceOreoCondition() {
//        if (Build.VERSION.SDK_INT >= 26) {
//            String CHANNEL_ID = "skw_service";
//            String CHANNEL_NAME = "SkyWay Background Service";
//
//            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
//                    CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
//            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
//
//            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
//                    .setCategory(Notification.CATEGORY_SERVICE).setSmallIcon(R.drawable.launcher_enable).setPriority(PRIORITY_MIN).build();
//            startForeground(101, notification);
//        }
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        context = this;
//        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mChatHeadView = LayoutInflater.from(this).inflate(R.layout.head_view, null);
//        initView();
//    }
//    void initView(){
//        loadFloatingBubble();
//        initView(params);
//    }
//
//    void loadFloatingBubble() {
//        int layout_type = -1;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            layout_type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
//        } else {
//            layout_type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
//        }
//
//        mChatHeadView = inflater.inflate(R.layout.head_view, null);
//        params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT,
//                WindowManager.LayoutParams.WRAP_CONTENT,
//                layout_type,
//                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
//                PixelFormat.TRANSLUCENT);
//
//        params.gravity = Gravity.TOP | Gravity.LEFT;
//        params.x = 0;
//        params.y = 50;
//
//        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (Settings.canDrawOverlays(context)){
//                mWindowManager.addView(mChatHeadView, params);
//            }
//        } else{
//            mWindowManager.addView(mChatHeadView, params);
//        }
//
//        Display display = mWindowManager.getDefaultDisplay();
//        final Point size = new Point();
//        display.getSize(size);
//
//        RelativeLayout chat_head_root = mChatHeadView.findViewById(R.id.chat_head_root);
//        ViewTreeObserver vto = chat_head_root.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                chat_head_root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                int width = chat_head_root.getMeasuredWidth();
//                //To get the accurate middle of the screen we subtract the width of the floating widget.
//                mWidth = size.x - width;
//            }
//        });
//    }
//
//    boolean isRight = false;
//    ImageView chatHeadImage;
//    MotionEvent tempEvent;
//
//    @SuppressLint("ClickableViewAccessibility")
//    private void initView(WindowManager.LayoutParams params) {
//        //Drag and move chat head using user's touch action.
//        chatHeadImage = (ImageView) mChatHeadView.findViewById(R.id.image_chat);
//        chatHeadImage.setOnClickListener(view -> {
//            checkLogin();
//        });
//        changeImageSize(false);
//        chatHeadImage.setOnTouchListener(new View.OnTouchListener() {
//            private int lastAction;
//            private int initialX;
//            private int initialY;
//            private float initialTouchX;
//            private float initialTouchY;
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                tempEvent = event;
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        Log.e(TAG, "onTouch: ACTION_DOWN");
//                        //remember the initial position.
//                        initialX = params.x;
//                        initialY = params.y;
//                        //get the touch location
//                        initialTouchX = event.getRawX();
//                        initialTouchY = event.getRawY();
//                        lastAction = event.getAction();
//                        changeImageSize(false);
//                        return true;
//                    case MotionEvent.ACTION_UP:
//                        Log.e(TAG, "onTouch: ACTION_UP");
//                        int diffPosicaoX = (int) (event.getRawX() - initialTouchX);
//                        int diffPosicaoY = (int) (event.getRawY() - initialTouchY);
//                        boolean singleClick = diffPosicaoX < 5 && diffPosicaoY < 5;
//                        if (singleClick) {
////                            String session = SharedUtils.getInstance(context).getStringValue(AppConstance.SESSION);
//                            checkLogin();
//
//                        }
//                        lastAction = event.getAction();
//                        int middle = mWidth / 2;
//                        float nearestXWall = params.x >= middle ? mWidth : 0;
//                        isRight = (nearestXWall == 0) ? false : true;
//                        params.x = (int) nearestXWall;
//                        mWindowManager.updateViewLayout(mChatHeadView, params);
//                        initCountdownTimer();
//                        return true;
//                    case MotionEvent.ACTION_MOVE:
//                        Log.e(TAG, "onTouch: ACTION_MOVE");
//                        changeImageSize(false);
//                        //Calculate the X and Y coordinates of the view.
//                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
//                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
//                        //Update the layout with new X & Y coordinate
//                        mWindowManager.updateViewLayout(mChatHeadView, params);
//                        lastAction = event.getAction();
//                        return true;
//                }
//                return false;
//            }
//        });
//        initCountdownTimer();
//    }
//
//    CountDownTimer countDownTimer;
//
//    int getDensity() {
//        DisplayMetrics displaymetrics = new DisplayMetrics();
//        mWindowManager.getDefaultDisplay().getMetrics(displaymetrics);
//        float value = getResources().getDisplayMetrics().density;
//        Log.e(TAG, "Screen Density = " + String.valueOf(value));
//        return Math.round(value);
//    }
//
//    void changeImageSize(boolean isHalf) {
//        int rotation = 0;
//        if (isHalf) {
//            rotation = (isRight) ? -90 : 90;
//            chatHeadImage.setImageResource(R.drawable.launcher_1_2);
//        } else {
//            rotation = 0;
//            chatHeadImage.setImageResource(R.drawable.launcher_enable);
//        }
//
//        chatHeadImage.setRotation(rotation);
//        chatHeadImage.getLayoutParams().height = (isHalf) ? (getDensity() * 30) : (getDensity() * 50);
//        chatHeadImage.getLayoutParams().width = (isHalf) ? (getDensity() * 30) : (getDensity() * 50);
////        chatHeadImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//        chatHeadImage.requestLayout();
//    }
//
//    void initCountdownTimer() {
//        if (countDownTimer == null) {
//            countDownTimer = new CountDownTimer(10000, 1000) {
//                @Override
//                public void onTick(long millisUntilFinished) {
//                    Log.e(TAG, "onTick: " + millisUntilFinished);
//                    if (millisUntilFinished < 6000) {
//                        //Change disable icon launcher
//                        chatHeadImage.setImageResource(R.drawable.skw_logo_disabled);
//                    }
//                    if (millisUntilFinished < 1000) {
//                        //Resize icon launcher
//                        changeImageSize(true);
//                        if (isRight){
//                            int middle = mWidth / 2;
//                            float nearestXWall = params.x >= middle ? (mWidth + chatHeadImage.getWidth()) : 0;
//                            params.x = (int) nearestXWall;
//                            mWindowManager.updateViewLayout(mChatHeadView, params);
//                        }
//                    }
//                }
//
//                @Override
//                public void onFinish() {
//
//                }
//            };
//        }
//        countDownTimer.start();
//    }
//
//    private void checkLogin() {
//        Intent intent = new Intent(BubbleViewService.this, SkyGameSdkAuthentActivity.class);
//        intent.putExtra("isClickStart", false);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (countDownTimer != null) {
//            countDownTimer.cancel();
//            countDownTimer = null;
//        }
//
//        if (mChatHeadView != null)
//            mWindowManager.removeView(mChatHeadView);
//    }
//
//    boolean isVerified = false;
//
//    @Override
//    public void resultSession(boolean success) {
//        isVerified = success;
////        if (isStart) {
////            checkLogin();
////        }
//    }
//}
