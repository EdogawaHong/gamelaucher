package com.skvn.authent.view.home;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;

//import com.facebook.AccessToken;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.HttpMethod;
//import com.facebook.internal.CallbackManagerImpl;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.google.android.gms.auth.api.signin.GoogleSignIn;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInClient;
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
//import com.google.android.gms.common.api.ApiException;
//import com.google.android.gms.tasks.OnCompleteListener;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.AuthCredential;
//import com.google.firebase.auth.AuthResult;
//import com.google.firebase.auth.FacebookAuthProvider;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;
//import com.google.firebase.auth.GoogleAuthProvider;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.identity.BeginSignInRequest;
import com.google.android.gms.auth.api.identity.BeginSignInResult;
import com.google.android.gms.auth.api.identity.Identity;
import com.google.android.gms.auth.api.identity.SignInClient;
import com.google.android.gms.auth.api.identity.SignInCredential;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.CustomToast;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.LoginInf;
import com.skvn.authent.model.base.CallbackResponse;
import com.skvn.authent.model.req.RegisterData;
import com.skvn.authent.presenter.LoginPresenter;
import com.skvn.authent.view.base.BaseActivity;
import com.skvn.authent.view.dialog.DialogListener;
import com.skvn.authent.view.headService.change_pass.ChangePassActivity;
import com.skvn.authent.view.register.RegisterActivity;

import java.util.Arrays;


public class HomeActivity extends BaseActivity implements LoginInf {
    private static final String TAG = "HomeActivity";
    EditText username;
    EditText password;
    AppCompatButton btnLogin, btnRegister;
    ImageButton btnLoginGuest, btnLoginGoogle, btnLoginFacebook;

    TextView tvForgotPassword;
    TextView tvVersion;
    ImageView imgShowPass;

    TextView txtErrorName, txtErrorPass;

    GoogleSignInClient mGoogleSignInClient;

    int REGISTER_USER = 1003;
    int RC_SIGN_IN = 1001;
    private static final int REQ_ONE_TAP = 1005;

    //    int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 9001;
    private boolean showPassword = false;

    private CallbackManager mCallbackManager;

    LoginPresenter presenter;

    private SignInClient oneTapClient;
    private BeginSignInRequest signInRequest;
    private BeginSignInRequest signUpRequest;

    private boolean showOneTapUI = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lib);
//        CustomToast.makeText(this, "Chào mừng vihahb đã đăng nhập!", CustomToast.LENGTH_LONG, 1).show();
        presenter = new LoginPresenter(getBaseContext(), this, new DialogListener() {
            @Override
            public void onPreload() {

            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onCancel() {

            }
        });
        initializingGoogleSDK();
        initializingFacebook();
        initializingGoogleOneTap();

        initView();
        Log.e(TAG, "SKYWAY SDK - LOGIN SCENE");
    }

    void initializingFacebook() {
        mCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                if (AccessToken.getCurrentAccessToken() == null) {
                    return; // already logged out
                }
                new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, graphResponse -> {
                    LoginManager.getInstance().logOut();
                    LoginManager.getInstance().logInWithReadPermissions(HomeActivity.this, Arrays.asList("public_profile,email"));
                }).executeAsync();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }

    void initializingGoogleOneTap() {
        oneTapClient = Identity.getSignInClient(this);

        signUpRequest = BeginSignInRequest.builder()
                .setGoogleIdTokenRequestOptions(BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                        .setSupported(true)
                        // Your server's client ID, not your Android client ID.
                        .setServerClientId("608594011911-e8v6g15bq3haj6s4gpsjjk074hrd9gdt.apps.googleusercontent.com")
                        // Show all accounts on the device.
                        .setFilterByAuthorizedAccounts(false)
                        .build())
                .build();

        signInRequest = BeginSignInRequest.builder()
                .setPasswordRequestOptions(BeginSignInRequest.PasswordRequestOptions.builder()
                        .setSupported(true)
                        .build())
                .setGoogleIdTokenRequestOptions(BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                        .setSupported(true)
                        // Your server's client ID, not your Android client ID.
                        .setServerClientId("608594011911-q766thtkpu9ts5l7vomqp4jp69440eb5.apps.googleusercontent.com")
                        // Only show accounts previously used to sign in.
                        .setFilterByAuthorizedAccounts(false)
                        .build())
                // Automatically sign in when exactly one credential is retrieved.
                .setAutoSelectEnabled(true)
                .build();
    }


    void initView() {
        username = findViewById(R.id.edtUsername);
        password = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.login);
        btnRegister = findViewById(R.id.register);
        btnLoginGuest = findViewById(R.id.login_guest);
        btnLoginGoogle = findViewById(R.id.login_google);
        btnLoginFacebook = findViewById(R.id.login_facebook);
        tvForgotPassword = findViewById(R.id.actionForgot);
        tvVersion = findViewById(R.id.version);
        imgShowPass = findViewById(R.id.imgShowPass);
        txtErrorName = findViewById(R.id.txtErrorName);
        txtErrorPass = findViewById(R.id.txtErrorPass);

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ChangePassActivity.class));
            }
        });

        imgShowPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPassword = !showPassword;
                if (showPassword) {
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    imgShowPass.setImageResource(R.drawable.icon_eyes_two);
                } else {
                    password.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    imgShowPass.setImageResource(R.drawable.icon_eyes_one);
                }
                password.setSelection(password.length());
                password.append("");
            }
        });
        registerReceiver(receiver, new IntentFilter(AppConstance.LOGIN_GUEST));
        registerReceiver(receiver, new IntentFilter(AppConstance.LOGIN_GOOGLE));
        registerReceiver(receiver, new IntentFilter(AppConstance.LOGIN_FACEBOOK));
        btnLogin.setOnClickListener(view -> {
//            if (validate()) {
//                SharedUtils.getInstance(this).putBooleanValue(AppConstance.TOKEN, true);
//                finish();
//            }
            presenter.checkInfo(username.getText().toString().trim(), password.getText().toString().trim());
        });
        btnRegister.setOnClickListener(view -> {
            Intent register = new Intent(this, RegisterActivity.class);
            startActivityForResult(register, REGISTER_USER);
        });
        btnLoginGoogle.setOnClickListener(view -> {
            checkUserGoogleLogin();
//            Utils.showToast(this, "Chức năng đang được phát triển!");
//            signInGoogle();
        });

        btnLoginFacebook.setOnClickListener(view -> {
//            Utils.showToast(this, "Chức năng đang được phát triển!");
            signInFacebook();
        });

        btnLoginGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading(true);
                presenter.loginGuest(HomeActivity.this);
            }
        });
    }

    private void signInGoogle() {
        Log.d(TAG, "signInGoogle: ");
        oneTapClient.beginSignIn(signUpRequest)
                .addOnSuccessListener(this, new OnSuccessListener<BeginSignInResult>() {
                    @Override
                    public void onSuccess(BeginSignInResult result) {
                        try {
                            startIntentSenderForResult(
                                    result.getPendingIntent().getIntentSender(), REQ_ONE_TAP,
                                    null, 0, 0, 0);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "Couldn't start One Tap UI: " + e.getLocalizedMessage());
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // No saved credentials found. Launch the One Tap sign-up flow, or
                        // do nothing and continue presenting the signed-out UI.
                        Log.d(TAG, "onFailure:" + e.getMessage());
                    }
                });
    }

    private void signInFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AppConstance.LOGIN_GUEST)) {
                presenter.loginGuest(HomeActivity.this);
            }

            if (intent.getAction().equals(AppConstance.LOGIN_GOOGLE)) {
                checkUserGoogleLogin();
            }

            if (intent.getAction().equals(AppConstance.LOGIN_FACEBOOK)) {
                signInFacebook();
            }
        }
    };

    boolean validate() {
        if (username.getText().toString() == null || username.getText().toString().equals("")) {
            Toast.makeText(this, "Tên tài khoản không hợp lệ!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (password.getText().toString() == null || password.getText().toString().equals("") || password.getText().toString().length() < 6) {
            Toast.makeText(this, "Mật khẩu không hợp lệ!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    void initializingGoogleSDK() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(getString(R.string.default_web_client_id))
                .requestIdToken("608594011911-5hlpcncj2kp1lmckf3mdqdeop0qorom1.apps.googleusercontent.com")
//                .requestServerAuthCode("608594011911-hq5aa7o4e72thvnf1qg32djml81vsrho.apps.googleusercontent.com")
                .requestEmail().requestId().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    void checkUserGoogleLogin() {
        try {
            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
            if (account != null && !account.isExpired()) {
                //account already login
//            Utils.showToast(this, "Bạn đã đăng nhập tài khoản Google rồi!");
                mGoogleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            loginGoogle();
                        }
                    }
                });
            } else {
                loginGoogle();
            }
        } catch (Exception e) {
            Utils.showToast(this, "Chức năng không khả dụng. Vui lòng thử lại sau!");
        }
    }

    void loginGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
//        startActivity(signInIntent);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d(TAG, "TokenAuthWithGoogle:" + account.getIdToken());
                //Push to server
                presenter.loginGoogle(HomeActivity.this, account.getIdToken());
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed: " + e.getMessage() + " " + e.getStatusCode(), e);
            }
        }

        if (requestCode == REQ_ONE_TAP) {
            try {
                SignInCredential credential = oneTapClient.getSignInCredentialFromIntent(data);
                String idToken = credential.getGoogleIdToken();
                if (idToken != null) {
                    Log.d(TAG, "Got ID token.");
                    Log.d(TAG, "TokenAuthWithGoogle:" + idToken);
                    //Push to server
                    presenter.loginGoogle(HomeActivity.this, idToken);
                }
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in (one tap) failed: " + e.getMessage() + " " + e.getStatusCode(), e);
            }
        }

        if (requestCode == REGISTER_USER) {
            if (data != null) {
                RegisterData registerData = (RegisterData) data.getSerializableExtra(AppConstance.REGISTER_RESPONSE);
                if (registerData != null) {
                    presenter.checkInfo(registerData.getUserName(), registerData.getPassword());
                }
            }
        }

        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            Log.e(TAG, "request login facebook");
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        presenter.loginFacebook(HomeActivity.this, token.getToken());
    }


    @Override
    public void inputError(boolean b, String errorName, String errorPass, String user_name, String pass) {
        if (!b) {
            if (errorName != null && !errorName.equals("")) {
                txtErrorName.setVisibility(View.VISIBLE);
                txtErrorName.setText(errorName);
            } else {
                txtErrorName.setVisibility(View.GONE);
                txtErrorName.setText("");
            }
            if (errorPass != null && !errorPass.equals("")) {
                txtErrorPass.setVisibility(View.VISIBLE);
                txtErrorPass.setText(errorPass);
            } else {
                txtErrorPass.setVisibility(View.GONE);
                txtErrorPass.setText("");
            }
        } else {
            txtErrorPass.setVisibility(View.GONE);
            txtErrorName.setVisibility(View.GONE);
            showLoading(true);
            presenter.login(this, user_name, pass);
//            if (validate()) {
////                SharedUtils.getInstance(this).putBooleanValue(AppConstance.SESSION, true);
////                finish();
//
//            }
        }
    }


    @Override
    public void loginSuccess(boolean success, String userName) {
        showLoading(false);
        if (success) {
            userNameTmp = userName;
            presenter.getInfo();
        }
    }

    String userNameTmp = "";

    @Override
    public void getInfoSuccess(boolean success, String userName) {
//        CallbackResponse response = new CallbackResponse(AppConstance.ACTION_LOGIN_SUCCESS, null);
//        Intent intent = new Intent();
//        intent.putExtra(AppConstance.CALLBACK_RESPONSE, response);
//        setResult(Activity.RESULT_OK, intent);
//        finish();
        String token = SharedUtils.getInstance(getApplicationContext()).getStringValue(AppConstance.TOKEN);
        CustomToast.makeText(this, "Chào mừng " + userNameTmp + " đã đăng nhập!", CustomToast.LENGTH_SHORT, 1).show();
        CallbackResponse response = new CallbackResponse(AppConstance.ACTION_LOGIN_SUCCESS, token);
        Intent intent = new Intent();
        intent.putExtra(AppConstance.CALLBACK_RESPONSE, response);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {
        CallbackResponse response = new CallbackResponse(AppConstance.ACTION_BACK, "login");
        Intent intent = new Intent();
        intent.putExtra(AppConstance.CALLBACK_RESPONSE, response);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}