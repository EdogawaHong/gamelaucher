package com.skvn.authent.view.register;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.skvn.authent.R;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.SharedUtils;
import com.skvn.authent.common.Utils;
import com.skvn.authent.listener.RegisterInf;
import com.skvn.authent.model.req.RegisterData;
import com.skvn.authent.presenter.RegisterPresenter;
import com.skvn.authent.view.base.BaseActivity;
import com.skvn.authent.view.dialog.DialogListener;

public class RegisterActivity extends BaseActivity implements RegisterInf {

    Button back, register;
    EditText edtUsername, edtPassword, edtRePassword;
    ImageView imgShowPass, imgShowConfirmPass;
    TextView txtErrorName, txtErrorPass, txtErrorConfirm;
    ImageButton login_guest;
    ImageButton login_google;
    ImageButton login_facebook;
    boolean showPass = false, showConfirmPass = false;

    RegisterPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register);

        try {
            findViewById(R.id.root).setOnTouchListener((View v, @SuppressLint("ClickableViewAccessibility") MotionEvent event) -> {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                if (getCurrentFocus().getWindowToken() != null) {
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
                return true;
            });
        } catch (Exception e) {

        }

        presenter = new RegisterPresenter(getBaseContext(), this, new DialogListener() {
            @Override
            public void onPreload() {

            }

            @Override
            public void onSuccess() {

            }

            @Override
            public void onCancel() {

            }
        });

        initView();
        Log.e(TAG, "SKYWAY SDK - REGISTER SCENE");
    }

    private static final String TAG = "RegisterActivity";

    void initView() {
        back = findViewById(R.id.exit);
        register = findViewById(R.id.register);
        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        edtRePassword = findViewById(R.id.edtIdentifier);

        login_guest = findViewById(R.id.login_guest);
        login_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast(new Intent(AppConstance.LOGIN_GUEST));
                finish();
            }
        });

        login_google = findViewById(R.id.login_google);
        login_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast(new Intent(AppConstance.LOGIN_GOOGLE));
                finish();
            }
        });

        login_facebook = findViewById(R.id.login_facebook);
        login_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendBroadcast(new Intent(AppConstance.LOGIN_FACEBOOK));
                finish();
            }
        });

        imgShowPass = findViewById(R.id.imgShowPass);
        imgShowConfirmPass = findViewById(R.id.imgShowConfirmPass);

        txtErrorName = findViewById(R.id.txtErrorName);
        txtErrorPass = findViewById(R.id.txtErrorPass);
        txtErrorConfirm = findViewById(R.id.txtErrorConfirmPass);

        back.setOnClickListener(view -> {
            finish();
        });

        register.setOnClickListener(view -> {
            presenter.checkInfo(edtUsername.getText().toString().trim(),
                    edtPassword.getText().toString().trim(),
                    edtRePassword.getText().toString().trim());
        });

        imgShowPass.setOnClickListener(view1 -> {
            showPass = !showPass;
            if (showPass) {
                edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                imgShowPass.setImageResource(R.drawable.icon_eyes_two);
            } else {
                edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                imgShowPass.setImageResource(R.drawable.icon_eyes_one);
            }
            edtPassword.setSelection(edtPassword.length());
        });

        imgShowConfirmPass.setOnClickListener(view1 -> {
            showConfirmPass = !showConfirmPass;
            if (showConfirmPass) {
                edtRePassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                imgShowConfirmPass.setImageResource(R.drawable.icon_eyes_two);
            } else {
                edtRePassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                imgShowConfirmPass.setImageResource(R.drawable.icon_eyes_one);
            }
            edtRePassword.setSelection(edtRePassword.length());
        });

        edtUsername.requestFocus();
    }

    boolean validate() {
        if (edtUsername.getText().toString() == null || edtUsername.getText().toString().equals("")) {
            Toast.makeText(this, "Tên tài khoản không hợp lệ!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (edtPassword.getText().toString() == null || edtPassword.getText().toString().equals("") || edtPassword.getText().toString().length() < 6) {
            Toast.makeText(this, "Mật khẩu không hợp lệ!", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (edtRePassword.getText().toString() == null || edtRePassword.getText().toString().equals("") || edtRePassword.getText().toString().length() < 6) {
            Toast.makeText(this, "Nhập lại mật khẩu không hợp lệ!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void inputError(boolean b, String errorName, String errorPass, String errorConfirm) {
        if (!b) {
            if (errorName != null && !errorName.equals("")) {
                txtErrorName.setVisibility(View.VISIBLE);
                txtErrorName.setText(errorName);
            } else {
                txtErrorName.setVisibility(View.GONE);
                txtErrorName.setText("");
            }
            if (errorPass != null && !errorPass.equals("")) {
                txtErrorPass.setVisibility(View.VISIBLE);
                txtErrorPass.setText(errorPass);
            } else {
                txtErrorPass.setVisibility(View.GONE);
                txtErrorPass.setText("");
            }
            if (errorConfirm != null && !errorConfirm.equals("")) {
                txtErrorConfirm.setVisibility(View.VISIBLE);
                txtErrorConfirm.setText(errorConfirm);
            } else {
                txtErrorConfirm.setVisibility(View.GONE);
                txtErrorConfirm.setText("");
            }
        } else {
            txtErrorName.setVisibility(View.GONE);
            txtErrorPass.setVisibility(View.GONE);
            txtErrorConfirm.setVisibility(View.GONE);
            showLoading(true);
            presenter.register(edtUsername.getText().toString().trim(),
                    edtPassword.getText().toString().trim());
        }
    }

    @Override
    public void registerSuccess(boolean success, String message, RegisterData registerData) {
        showLoading(false);
        if (success) {
            Utils.showToast(this, "Đăng ký thành công!");
            Intent intent = new Intent();
            intent.putExtra(AppConstance.REGISTER_RESPONSE, registerData);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            if (message != null && message.length() > 0)
                Utils.showToast(this, message);
        }
    }
}