package com.mongbavuong.skyway;

import android.app.Application;
import android.content.Context;
import android.util.Log;

public class GameApplication extends Application {
    static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        // Simply add the handler, and that's it! No need to add any code
        // to every activity. Everything is contained in MyLifecycleHandler
        // with just a few lines of code. Now *that's* nice.
//        registerActivityLifecycleCallbacks(new AppLifecycleHandler());
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Log.e("Alert","Lets See if it Works !!!");
            }
        });
    }
}
