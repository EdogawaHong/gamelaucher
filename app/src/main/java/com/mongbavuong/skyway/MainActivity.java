package com.mongbavuong.skyway;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.skvn.authent.common.AppConstance;
import com.skvn.authent.common.Utils;
import com.skvn.authent.iap.IAPPaymentResult;
import com.skvn.authent.iap.IAPUtils;
import com.skvn.authent.listener.ActionCallback;
import com.skvn.authent.model.base.CallbackResponse;
import com.skvn.authent.view.SkyGameSdkAuthentActivity;

import java.util.ArrayList;
//import com.skvn.authent.view.headService.LifeCycleReceiver;


public class MainActivity extends AppCompatActivity {
    static Activity app;
    private static final String TAG = "MainActivity";
    Button button2;
    Button button3;


    void clickToHeadLauncher() {

        Intent intent = new Intent(MainActivity.this, SkyGameSdkAuthentActivity.class);
        intent.putExtra("isClickStart", false);
        ActionCallback result = new ActionCallback() {
            @Override
            public void onCompleteAction(CallbackResponse response) {
                switch (response.getKey()) {
                    case AppConstance.ACTION_LOGOUT:
                        //Todo("Put the code to hide “Head Launcher” to here")
                        break;
                }
            }
        };
        SkyGameSdkAuthentActivity.setCompletedCallback(result);
        startActivity(intent);

    }


    @Override
    protected void onStart() {
        super.onStart();
    }
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = this;
        mAuth = FirebaseAuth.getInstance();
        //Khi moi khoi dong game, can shoư login tu sdk
        showLauncher(false);


        //Khi da login, can show Trong thong tin tu SDK
        //Dai dien cho hanh dong click vao bubble view
        button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLauncher(true);
            }
        });

        ArrayList<String> skuList = new ArrayList<>();
        skuList.add("99cent_buy_gold");
        skuList.add("4699cent_buy_gold");
        IAPUtils.getInstance().initializingIAP(this, skuList);

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String serverId = "1";
                String gameClientTransId = "1";
                String userId = "1";
                String productCode = "99cent_buy_gold";

                IAPPaymentResult result = new IAPPaymentResult() {
                    @Override
                    public void notFoundSku() {
                        //Hàm thông báo khi không tìm thấy sản phẩm cần mua.
                        Toast.makeText(MainActivity.app, "notFoundSku", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        //Hàm thông báo khi người dùng hủy bỏ luồng mua IAP
                        Toast.makeText(MainActivity.app, "onCancel", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onSuccess() {
                        //Hàm thông báo khi người dùng mua thành công sản phẩm IAP
                        Toast.makeText(MainActivity.app, "onSuccess", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailed(String message) {
                        //Hàm thông báo khi luồng mua IAP gặp sự cố phát sinh lỗi
                        Toast.makeText(MainActivity.app, "onFailed " + message, Toast.LENGTH_SHORT).show();
                    }
                };

                IAPUtils.getInstance().buyProduct(serverId, gameClientTransId, userId, productCode, result);
            }
        });
    }


    //Dai dien cho logic cua SDk
    void showLauncher(boolean clickFromBubbleView) {
        Intent intent = new Intent(MainActivity.this, SkyGameSdkAuthentActivity.class);
        intent.putExtra("isClickStart", !clickFromBubbleView);
        ActionCallback result = new ActionCallback() {
            @Override
            public void onCompleteAction(CallbackResponse response) {
                switch (response.getKey()) {
                    case AppConstance.ACTION_LOGOUT:
                        //Hàm thông báo người dùng đăng xuất khỏi Launcher.
                        //Đối tác có thể chuyển user game ra splash screen.
                        //SDK sẽ tự động bật màn hình Login. Đối tác không cần gọi màn hình login ở hàm này.
                        Log.e(TAG, "onCompleteAction: " + AppConstance.ACTION_LOGOUT);
                        Utils.showToast(getApplicationContext(), "onCompleteAction: " + AppConstance.ACTION_LOGOUT);
                        break;
                    case AppConstance.ACTION_BACK:
                        //Hàm thông báo khi người dùng nhấn back từ màn hình login.
                        //Đối tác có thể cho thoát game nếu người dùng không chịu đăng nhập
                        Log.e(TAG, "onCompleteAction: " + AppConstance.ACTION_BACK + " - " + response.getValue());
                        Utils.showToast(getApplicationContext(), "onCompleteAction: " + AppConstance.ACTION_BACK);
                        finish();
                        break;
                    case AppConstance.ACTION_LOGIN_SUCCESS:
                        //Hàm thông báo khi người dùng đăng nhập thành công hoặc đã đăng nhập(khi khởi động lại game).
                        Log.e(TAG, "onCompleteAction: " + AppConstance.ACTION_LOGIN_SUCCESS + " - " + response.getValue());
                        Utils.showToast(getApplicationContext(), "onCompleteAction: " + AppConstance.ACTION_LOGIN_SUCCESS);
                        break;
                }
            }
        };
        SkyGameSdkAuthentActivity.setCompletedCallback(result);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        SharedUtils.getInstance(app).putStringValue(AppConstance.MAIN_ACTIVITY_LAUNCHER, this.getLocalClassName());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}